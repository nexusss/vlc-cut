import QtQuick 2.0
import QtQuick.Layouts 1.3
import QmlVlc 0.1
import QtMultimedia 5.0
import QtGraphicalEffects 1.0
import "rotatorCalc.js" as Rotator
import "JS/surfPlay.js" as SurfPlay

Rectangle {

    id: selComp
    parent: vlcTransportenSurf
    property real startTime: 0
    property real stopTime: 0
    property int index: 0
    property string source: ""
    property string musicSource: ""
    property int rotatorX: x
    property int rotatorY: y
    property int type: 0
    property string textstring: ""
    property string fontName: "Verdana"
    property string textColor: "black"
    property string textHighlightColor: "#f9ee13"
    property bool textHighlight: false
    property int blurLevel: 50
    property int volumeLevel: 100
    property bool playstatus: false

    signal geomChange(int index,int x, int y,int width,int height,int angle)
    signal geomPreviewChange(int index,int x, int y,int width,int height)
    signal timesChange(int index,real startTime,real stopTime)
    signal textChange(int index,string text,string color,string fontName,bool highlightState,string highlightColor)

    signal blurChange(int index,int blurLevel)

    function setRect(x,y,width,height,angle){
        selComp.x = x
        selComp.y = y
        selComp.width = width
        selComp.height = height
        selComp.rotation = angle
    }

    onVisibleChanged: {
        SurfPlay.visible = visible
        SurfPlay.checkPlayStatus(volumeLevel)
    }

    onPlaystatusChanged:{
        SurfPlay.play = playstatus
        SurfPlay.checkPlayStatus(volumeLevel)
    }

    onFontNameChanged: {
        if(type == 1){
            text1.font.family = fontName
            textChange(index,textstring,textColor,fontName,textHighlight,textHighlightColor)
        }
    }

    onTextHighlightChanged: {
        textChange(index,textstring,textColor,fontName,textHighlight,textHighlightColor)
    }

    onTextHighlightColorChanged: {
        textChange(index,textstring,textColor,fontName,textHighlight,textHighlightColor)
    }

            onBlurLevelChanged: {
                blurChange(index,blurLevel)
                blur.radius = blurLevel
            }

            onXChanged: {
                var rect = Rotator.calcNewMaxXY(x,y,width,height,rotation)
                rotatorX = rect.maxX
                rotatorY = rect.maxY
                mdragArea.recalcMinMax()
                geomChange(index,rotatorX,rotatorY,width,height,rotation)
                geomPreviewChange(index,x,y,width,height)
                if(type == 2){
                    effectSource.sourceRect = Qt.rect(x,selComp.y, selComp.width, selComp.height)
                }
            }
            onYChanged: {
                var rect = Rotator.calcNewMaxXY(x,y,width,height,rotation)
                rotatorX = rect.maxX
                rotatorY = rect.maxY
                mdragArea.recalcMinMax()
                geomChange(index,rotatorX,rotatorY,width,height,rotation)
                geomPreviewChange(index,x,y,width,height)
                if(type == 2){
                    effectSource.sourceRect = Qt.rect(selComp.x,y, selComp.width, selComp.height)
                }
            }
            onWidthChanged: {
                var rect = Rotator.calcNewMaxXY(x,y,width,height,rotation)
                rotatorX = rect.maxX
                rotatorY = rect.maxY
                mdragArea.recalcMinMax()
                geomChange(index,rotatorX,rotatorY,width,height,rotation)
                geomPreviewChange(index,x,y,width,height)
                if(type == 2){
                    effectSource.sourceRect = Qt.rect(selComp.x,selComp.y,width, selComp.height)
                }
            }
            onHeightChanged: {
                var rect = Rotator.calcNewMaxXY(x,y,width,height,rotation)
                rotatorX = rect.maxX
                rotatorY = rect.maxY
                mdragArea.recalcMinMax()
                geomChange(index,rotatorX,rotatorY,width,height,rotation)
                geomPreviewChange(index,x,y,width,height)
                if(type === 1){
                    text1.font.pixelSize = height
                }
                else if(type == 2){
                    effectSource.sourceRect = Qt.rect(selComp.x,selComp.y, selComp.width, height)
                }

            }

            onTextstringChanged:  {
                if(type == 1){
                    text1.text = qsTr(textstring)
                    textChange(index,textstring,textColor,fontName,textHighlight,textHighlightColor)
                }
            }

            onTextColorChanged: {
                if(type == 1){
                    text1.color = textColor
                    textChange(index,textstring,textColor,fontName,textHighlight,textHighlightColor)
                }
            }

            onTypeChanged: {
                if(type == 0){
                    xLeftRec.visible = true
                    xRightRec.visible = true
                    rotateRec.visible = true
                }
                else if(type == 1){
                    xLeftRec.visible = false
                    xRightRec.visible = false
                    rotateRec.visible = true
                }
                else if(type == 2){
                    rotateRec.visible = false
                }
                else if(type == 3){
                    xLeftRec.visible = false
                    xRightRec.visible = false
                    rotateRec.visible = false
                    upRec.visible = false
                    downRec.visible = false
                    selComp.border.width = 0
                }

                stack.currentIndex = type
            }

            onStartTimeChanged: timesChange(index,startTime,stopTime)
            onStopTimeChanged: timesChange(index,startTime,stopTime)

            onSourceChanged: {
                image.source = source
            }

            onIndexChanged: {
                selComp.z = 0 - index
            }

            border {
                width: 1
                color: "steelblue"
            }
            color: "transparent"

            StackLayout{
                id: stack
                Layout.fillWidth: true
                Layout.fillHeight: true
                currentIndex: type
                anchors.fill: parent

                Image {
                    id: image

                    source:source


                    anchors.fill: parent
                    onSourceSizeChanged: {
                        if(selComp.width == 0)
                            selComp.width = sourceSize.width
                        if(selComp.height == 0)
                            selComp.height = sourceSize.height
                    }
                }


                Text {
                    id:text1
                    font.family: fontName
                    font.pixelSize: selComp.height
                    color: textColor
                    style: Text.Normal
                    verticalAlignment: Text.AlignVCenter

                    onContentSizeChanged: {
                        if(type === 1)
                            selComp.width = contentWidth
                    }
                    Rectangle{
                        z: -1
                        anchors.fill: parent
                        color: textHighlightColor
                        visible: textHighlight
                    }
                }

                Item{
                    id: blurItem

                    ShaderEffectSource {
                            id: effectSource
                            sourceItem: vlcSurface
                            anchors.fill: parent
                            sourceRect: Qt.rect(selComp.x,selComp.y, selComp.width, selComp.height)

                        }

                     FastBlur{
                            id: blur
                            anchors.fill: effectSource

                            source: effectSource
                            radius: blurLevel

                            anchors.rightMargin: 0
                            anchors.bottomMargin: 0
                            anchors.leftMargin: 0
                            anchors.topMargin: 0
                        }
                }
                Item {
                    id: music
                    VlcPlayer{
                        id: vlc
                        mrl: musicSource
                        volume: 0
                        onMediaPlayerEndReached: {
                            time = 0
                            SurfPlay.checkPlayStatus(volumeLevel)
                        }
                        onMediaPlayerOpening: SurfPlay.checkPlayStatus(volumeLevel)
                    }
                }
            }

            property int rulersSize: 10

            MouseArea {     // drag mouse area
                id: mdragArea
                anchors.fill: parent
                drag{
                    target: parent
                    minimumX: 0
                    minimumY: 0
                    maximumX: parent.parent.width - parent.width
                    maximumY: parent.parent.height - parent.height
                }
                function recalcMinMax(){
                    mdragArea.drag.minimumX = parent.x - rotatorX
                    mdragArea.drag.minimumY = parent.y - rotatorY
                    mdragArea.drag.maximumX = parent.parent.width - parent.width - drag.minimumX
                    mdragArea.drag.maximumY = parent.parent.height - parent.height - drag.minimumY
                }
            }

            Rectangle {
                id: xLeftRec
                width: rulersSize
                height: rulersSize
                radius: rulersSize
                color: "steelblue"
                anchors.horizontalCenter: parent.left
                anchors.verticalCenter: parent.verticalCenter
                z: 10
                MouseArea {
                    anchors.fill: parent
                    drag{
                        target: parent; axis: Drag.XAxis

                    }

                    onMouseXChanged: {
                        if(drag.active){

                            if(selComp.x + mouseX >= mdragArea.drag.minimumX &&
                                    selComp.width - mouseX >= 3){
                                selComp.width = selComp.width - mouseX
                                selComp.x = selComp.x + mouseX
                            }
                        }
                    }
                }
            }

            Rectangle {
                id: xRightRec
                width: rulersSize
                height: rulersSize
                radius: rulersSize
                color: "steelblue"
                anchors.horizontalCenter: parent.right
                anchors.verticalCenter: parent.verticalCenter
                z: 10
                MouseArea {
                    anchors.fill: parent
                    drag{ target: parent; axis: Drag.XAxis }
                    onMouseXChanged: {
                        if(drag.active){
                            if(selComp.x + selComp.width + mouseX <= selComp.parent.width + mdragArea.drag.minimumX  &&
                                    selComp.width + mouseX >= 3)
                                selComp.width = selComp.width + mouseX
                        }
                    }
                }
            }

            Rectangle {
                id: upRec
                width: rulersSize
                height: rulersSize
                radius: rulersSize
                x: parent.x / 2
                y: 0
                color: "steelblue"
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.top
                z: 10
                MouseArea {
                    anchors.fill: parent
                    drag{ target: parent; axis: Drag.YAxis }
                    onMouseYChanged: {
                        if(drag.active){
                            if(selComp.y + mouseY >= mdragArea.drag.minimumY&&
                                    selComp.height - mouseY >= 5){
                                selComp.height = selComp.height - mouseY
                                selComp.y = selComp.y + mouseY
                            }
                        }
                    }
                }
            }


            Rectangle {
                id: downRec
                width: rulersSize
                height: rulersSize
                radius: rulersSize
                x: parent.x / 2
                y: parent.y
                color: "steelblue"
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.bottom
                z: 10
                MouseArea {
                    anchors.fill: parent
                    drag{target: parent; axis: Drag.YAxis}
                    onMouseYChanged: {
                        if(drag.active){

                            if(selComp.y + selComp.height + mouseY <= selComp.parent.height + mdragArea.drag.minimumY &&
                                selComp.height + mouseY >= 5)
                                selComp.height = selComp.height + mouseY
                        }
                    }
                }
            }
            Rectangle {
                id: rotateRec
                width: rulersSize * 2
                height: rulersSize * 2
                radius: rulersSize
                color: "#00000000"
                x:0 - image1.sourceSize.width /2
                y:0 - image1.sourceSize.height / 2

                z: 10
                Image {
                    id: image1
                    anchors.fill: parent
                    source: "rotate_arrow.png"

                }

                MouseArea{
                    anchors.fill: parent;
                    onPositionChanged:  {
                        var truex = mouseX-selComp.width/2
                        var truey = selComp.height/2-mouseY
                        var angle = Math.atan2(truex, truey)
                        selComp.rotation += 90 + angle * 180 / Math.PI
                        if(selComp.rotation >= 360)
                            selComp.rotation -= 360

                        effectSource.rotation = selComp.rotation

                        var rect = Rotator.calcNewMaxXY(selComp.x,selComp.y,selComp.width,selComp.height,selComp.rotation)
                        rotatorX = rect.maxX
                        rotatorY = rect.maxY
                        mdragArea.recalcMinMax()
                        geomChange(selComp.index,rotatorX,rotatorY,selComp.width,selComp.height,selComp.rotation)
                        geomPreviewChange(index,x,y,width,height)
                    }
                }
            }
    function play(){
        if(type === 3){
            playstatus = true
        }
    }
    function pause(){
        if(type === 3){            
            playstatus = false
        }
    }

    function stop(){
        if(type === 3){
            playstatus = false
            SurfPlay.stopplay()
        }
    }

    function updateVolume(volume){
        volumeLevel = volume
        vlc.volume = volume
    }

    function posChange(pos){
        if(type === 3){
            console.log("pos",pos,pos - startTime)
            SurfPlay.posChanged(pos - startTime)
        }
    }
}
