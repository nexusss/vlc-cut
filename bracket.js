
function createBracket(type,value,maximumValue,width,height,object) {
    var valuePos = value / maximumValue
                var xPos = width * ( valuePos)
                var yPos = 0


                var bracketWidth = (76 / 161) * height;
                var bracketHeight = height
                var component = Qt.createComponent(type)
                if (component.status == Component.Ready) {
                    var childRec = component.createObject(object)
                    if(type == "BracketClose.qml")
                        childRec.x = xPos - (bracketWidth / 2)
                    else
                        childRec.x = xPos + (bracketWidth / 2)

                    childRec.y = yPos
                    childRec.widthImage = bracketWidth
                    childRec.heightImage = bracketHeight
                    childRec.value = value
                    return childRec;
                }

}
