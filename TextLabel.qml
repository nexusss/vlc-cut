import QtQuick 2.0

Item {
    id: item1
    width: 30
    height: 30
    property var text:""
    onTextChanged: text1.text = text
    Rectangle {
            id: rectangle1
            x: 0
            width: 5
            height: 16
            color: "#cbcbcb"
            radius: 2
            border.color: "#888888"
            anchors.top: parent.top
            anchors.topMargin: 0
            anchors.horizontalCenter: parent.horizontalCenter
        }

        Text {
            id: text1
            text: text
            color: "white"
            anchors.left: parent.left
            anchors.leftMargin: 0
            anchors.right: parent.right
            anchors.rightMargin: 0
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 0
            font.pixelSize: 12
        }

}
