import QtQuick 2.0

import "createBracket.js" as BracketJS

Rectangle {
    id: timebar
    opacity: 1
    height: 30
    color: "#00000000"

    property int maxValue: 0

    onMaxValueChanged: createLabelTime()

    onWidthChanged: createLabelTime()
    ListModel{
        id: labelsModel
        property var labels
    }

    Rectangle {
        id: timeline
        height: 4

        color: "#cbcbcb"
        radius: 2
        border.color: "#888888"
        anchors.left: parent.left
        anchors.leftMargin: 6
        anchors.right: parent.right
        anchors.rightMargin: 3
        border.width: 1

        y: 6
    }

    function createLabelTime(){
        for(var i = 0; i < labelsModel.count; i++){
            labelsModel.get(i).labels.destroy()
        }

        labelsModel.clear()

        var defNumberOfLabel = 15
        var minValue = 5
        var timeLabel = maxValue / defNumberOfLabel
        var newMaxValue = maxValue

        timeLabel = Math.round(timeLabel)
        if(timeLabel < 5)
            timeLabel = minValue

        if(timeLabel >= 60 && timeLabel < 3600){
            timeLabel /= 60 //convert to minutes
            newMaxValue /= 60
        }
        else if(timeLabel > 3600){
            timeLabel /= 3600 // convert ot hours
            newMaxValue /= 3600
        }

        var lastNumber = timeLabel % 10
        if(lastNumber > 0 && lastNumber <= 2){
            timeLabel -= lastNumber
        }
        else if(lastNumber > 2 && lastNumber < 5){
            timeLabel += (5 - lastNumber)
        }
        else if(lastNumber > 5 && lastNumber <= 7){
            timeLabel -= (lastNumber - 5)
        }
        else if(lastNumber > 7 ){
            timeLabel += (10 - lastNumber)
        }

        if(timeLabel === 0)
            timeLabel = 1

        var numberOfLabel = Math.ceil(newMaxValue / timeLabel)
        for(var i = 1; i < numberOfLabel; i++){
            var component = Qt.createComponent("TextLabel.qml")
            if (component.status === Component.Ready) {
                var childRec = component.createObject(timeBar)
                var valuePos = timeLabel * i / newMaxValue
                var xPos = ((timeline.width) * ( valuePos)) - (timeBar.width - timeline.width)


                childRec.x = xPos
                childRec.y = 0
                childRec.text = timeLabel * i
                labelsModel.append({"labels":childRec})
            }
        }
    }
}
