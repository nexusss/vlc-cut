#include "filelist.h"
#include <QFileDialog>
#include <QDir>

FileList::FileList(QObject *qmlFile, QObject *parent) : QObject(parent),qmlFile(qmlFile)
{
    if(qmlFile == 0)
        return;

    QDir musicDir;
    musicDir.setPath("Music");
    QStringList filter;
    filter << "*.mp3" << "*.ogg";

    filesList = musicDir.entryList(QStringList() << "*.mp3" << "*.ogg",QDir::Files);
    for (int i = 0; i < filesList.size(); i++)
        filesList[i] = "file:///" + musicDir.absolutePath() + "/" + filesList.at(i);

    qmlFile->setProperty("folderModel", QVariant::fromValue(filesList));

}
