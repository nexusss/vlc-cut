import QtQuick 2.0
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtQuick.Layouts 1.3
import QmlVlc 0.1
import QtMultimedia 5.5
import Qt.labs.folderlistmodel 2.1
import QtQuick.Dialogs 1.2
import "converSeconds.js" as ConvertSeconds
import "JS/GetFileName.js" as GetFileName
import "style"

Rectangle {
    visible: true
    color: "#222222"

    signal addMusicSignal(string path,string fileName,int musicIndex,real length)
    signal remMusicSignal(int surfIndex)

    signal openFileDialog(string path)

    property var folderModel: 0
    onFolderModelChanged: {
        for(var i = 0; i < folderModel.length; i++)
            model.append({fileURL: folderModel[i].toString(),fileName: GetFileName.getFileName(folderModel[i].toString())})
    }

    ListModel{
        id: model

    }

    VlcPlayer {
        id: vlcMusicPlayer

    }

    FileDialog {
         id: fileDialog
         title: "Please choose a file"
         nameFilters: [ "Music files (*.mp3 *.ogg)" ]
         folder: shortcuts.home
         onAccepted: model.append({fileURL: fileUrl.toString(),fileName:GetFileName.getFileName(fileUrl.toString())})
     }


    ColumnLayout {
        id: columnLayout1
        anchors.fill: parent

        ScrollView {
            width: parent.width
            height: parent.height
            Layout.fillWidth: true
            Layout.fillHeight: true
            frameVisible: true

            style: ScrollViewStyle {
                handle: Item {
                    implicitWidth: 14
                    implicitHeight: 26
                    Rectangle {
                        color: "#424246"
                        anchors.fill: parent
                    }
                }
                scrollBarBackground: Item {
                    implicitWidth: 14
                    implicitHeight: 26
                }
            }

            ListView {
                id: musicListView
                anchors.fill: parent

                highlight:Rectangle{

                    width: parent.width
                    height: 35
                    color:"#1EA286"
                    radius: 5
                    opacity: 0.5
                    focus: true
                    y: musicListView.currentItem.y;
                    Behavior on y { SpringAnimation { spring: 2; damping: 0.1 } }
                }

                highlightFollowsCurrentItem: false
                focus: true
                onCurrentItemChanged: {
                    addRemBut.currentIndex = musicListView.currentItem.added ? 1 : 0
                    if(vlcMusicPlayer.playing)
                        vlcMusicPlayer.play(musicListView.currentItem.getFullPath())
                }

                model: model
                delegate: Rectangle {

                    width: parent.width
                    height: 35
                    radius: 5
                    color: "#00000000"
                    property string pathStr : fileURL
                    property bool added: false
                    property int surfIndex: -1

                    RowLayout {
                        anchors.fill: parent

                        Label {
                            id: pathL
                            text: fileName
                            font.pointSize: 12
                            verticalAlignment: Text.AlignVCenter
                            horizontalAlignment: Text.AlignLeft
                            Layout.fillHeight: true
                            Layout.fillWidth: true
                            color: "white"
                            clip: true
                        }

                        Label {
                            id: adedL
                            height: 35
                            text: added ? "added" : ""
                            font.pointSize: 12
                            verticalAlignment: Text.AlignVCenter
                            horizontalAlignment: Text.AlignLeft
                            Layout.fillHeight: true
                            Layout.minimumWidth: 40
                            color: "white"
                        }
                    }
                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            musicListView.currentIndex = index
                        }
                    }
                    function getFullPath(){
                        return pathStr
                    }
                    function getFileName(){
                        return pathL.text
                    }
                }

            }
            Component.onCompleted: musicListView.currentIndex = 0
        }

        RowLayout {
            Layout.fillHeight: true
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
            Layout.fillWidth: true
            Label {
                    id: curTimeLabel
                    text: ConvertSeconds.toHHMMSS(vlcMusicPlayer.time / 1000)//qsTr("00:00:00.000")
                    color: "white"
                    verticalAlignment: Text.AlignVCenter
                    font.bold: false
                    font.pointSize: 10
                    horizontalAlignment: Text.AlignRight
            }

                Label {
                    id: label1
                    text: qsTr("/")
                    color: "white"
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                    font.bold: true
                    font.pointSize: 10
                }



                Label {
                    id: allTimeLabel
                    text: ConvertSeconds.toHHMMSS(vlcMusicPlayer.length / 1000)
                    verticalAlignment: Text.AlignVCenter
                    color: "white"
                    font.bold: false
                    font.pointSize: 10
                    horizontalAlignment: Text.AlignLeft
                }
            Button {
                id: playB
                Layout.minimumWidth: playI.width
                Layout.maximumWidth: playI.width
                Layout.minimumHeight: playI.height

                style: MyButtonStyle{}/*ButtonStyle {

                    background: Rectangle {

                        radius: 4
                        gradient: Gradient {
                            GradientStop { position: 0 ; color: control.pressed ? "dimgrey" : "darkgrey" }
                            GradientStop { position: 1 ; color: control.pressed ? "dimgrey" : "darkgrey" }
                        }
                    }

                }*/
                Image {
                    id: playI
                    x: 0
                    y: 0
                    width: sourceSize.width
                    height: sourceSize.height
                    source: "icons/playM.png"
                }
                onClicked: {
                    vlcMusicPlayer.play(musicListView.currentItem.getFullPath())
                }
            }

            Button {
                id: pauseB

                Layout.minimumWidth: pauseI.width
                Layout.maximumWidth: pauseI.width

                Layout.minimumHeight: pauseI.height

                style: MyButtonStyle{}/*ButtonStyle {
                    background: Rectangle {

                        radius: 4
                        gradient: Gradient {
                            GradientStop { position: 0 ; color: control.pressed ? "dimgrey" : "darkgrey" }
                            GradientStop { position: 1 ; color: control.pressed ? "dimgrey" : "darkgrey" }
                        }
                    }

                }*/
                Image {
                    id: pauseI
                    x: 0
                    y: 0
                    width: sourceSize.width
                    height: sourceSize.height
                    source: "icons/pauseM.png"
                }
                onClicked: vlcMusicPlayer.pause()
            }

            Button {
                id: stopB
                Layout.minimumWidth: stopI.width
                Layout.maximumWidth: stopI.width
                Layout.minimumHeight: stopI.height
                style: MyButtonStyle{}/*ButtonStyle {
                    background: Rectangle {

                        radius: 4
                        gradient: Gradient {
                            GradientStop { position: 0 ; color: control.pressed ? "dimgrey" : "darkgrey" }
                            GradientStop { position: 1 ; color: control.pressed ? "dimgrey" : "darkgrey" }
                        }
                    }

                }*/
                Image {
                    id: stopI
                    x: 0
                    y: 0
                    width: sourceSize.width
                    height: sourceSize.height
                    source: "icons/stopM.png"
                }
                onClicked: vlcMusicPlayer.stop()
            }

            Button {
                id: importbutton
                text: qsTr("Import")
                style: MyButtonStyle{}/*ButtonStyle {
                    background: Rectangle {
                        border.width: control.activeFocus ? 2 : 1
                        border.color: "#888"

                        radius: 4
                        gradient: Gradient {
                            GradientStop { position: 0 ; color: control.pressed ? "dimgrey" : "darkgrey" }
                            GradientStop { position: 1 ; color: control.pressed ? "dimgrey" : "darkgrey" }
                        }
                    }
                    label: Text {
                        color: "white"
                        text: qsTr("Import")
                        verticalAlignment: Text.AlignVCenter
                        horizontalAlignment: Text.AlignHCenter
                    }
                }*/
                onClicked: {
                    fileDialog.open()
                }
            }

            StackLayout{
                id: addRemBut
                Layout.fillWidth: false
                Layout.fillHeight: false
                Button {
                    id: addbutton
                    Layout.fillHeight: true
                    text: qsTr("Add")
                    style: MyButtonStyle{}/*ButtonStyle {
                        background: Rectangle {
                            border.width: control.activeFocus ? 2 : 1
                            border.color: "#888"

                            radius: 4
                            gradient: Gradient {
                                GradientStop { position: 0 ; color: control.pressed ? "dimgrey" : "darkgrey" }
                                GradientStop { position: 1 ; color: control.pressed ? "dimgrey" : "darkgrey" }
                            }
                        }
                        label: Text {
                            color: "white"
                            text: qsTr("Add")
                            verticalAlignment: Text.AlignVCenter
                            horizontalAlignment: Text.AlignHCenter
                        }
                    }*/
                    onClicked: {
                        musicListView.currentItem.added = true
                        vlcMusicPlayer.mrl = musicListView.currentItem.getFullPath()
                        vlcMusicPlayer.volume = 0
                        vlcMusicPlayer.mediaPlayerLengthChanged.connect(addButtonSlot)
                        addRemBut.currentIndex = 1
                    }
                }
                Button {
                    id: rembutton
                    Layout.fillHeight: true
                    text: qsTr("Remove")
                    style: MyButtonStyle{}/*ButtonStyle {
                        background: Rectangle {
                            border.width: control.activeFocus ? 2 : 1
                            border.color: "#888"

                            radius: 4
                            gradient: Gradient {
                                GradientStop { position: 0 ; color: control.pressed ? "dimgrey" : "darkgrey" }
                                GradientStop { position: 1 ; color: control.pressed ? "dimgrey" : "darkgrey" }
                            }
                        }
                        label: Text {
                            color: "white"
                            text: qsTr("Remove")
                            verticalAlignment: Text.AlignVCenter
                            horizontalAlignment: Text.AlignHCenter
                        }
                    }*/
                    onClicked: {
                        removeMusic(musicListView.currentIndex)
                        remMusicSignal(musicListView.currentItem.surfIndex)
                    }
                }
            }
        }
    }

    function removeMusic(remIndex){
        var bakItem = musicListView.currentIndex
        musicListView.currentIndex = remIndex
        musicListView.currentItem.added = false
        musicListView.currentIndex = bakItem
        addRemBut.currentIndex = 0
    }

    function setSurfIndex(musicIndex,surfIndex){
        var bakItem = musicListView.currentIndex
        musicListView.currentIndex = musicIndex
        musicListView.currentItem.surfIndex = surfIndex
        musicListView.currentIndex = bakItem
    }

    function addButtonSlot(){
        addMusicSignal(musicListView.currentItem.getFullPath(),
                 musicListView.currentItem.getFileName(),
                 musicListView.currentIndex,
                 vlcMusicPlayer.length / 1000)


        vlcMusicPlayer.pause()
        vlcMusicPlayer.volume = 100
        vlcMusicPlayer.mrl = ""
        vlcMusicPlayer.mediaPlayerLengthChanged.disconnect(addButtonSlot)
    }
}
