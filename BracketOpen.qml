import QtQuick 2.0

Item {
    width: 75
    height: 160
    property real value: 0
    property string color : "white"
    property bool dragEnabled : false

    signal posChange(int x)

    property int maxX: parent.width - width
    property int minX: 0

    onMaxXChanged: mouseArea.drag.maximumX = maxX
    onMinXChanged: mouseArea.drag.minimumX = minX

//    onXChanged: posChange(x)


    onDragEnabledChanged: mouseArea.enabled = dragEnabled

    onColorChanged: {
        rectangle1.color = color
        rectangle2.color = color
        rectangle3.color = color
    }

    onWidthChanged: {
        rectangle1.width = width
        rectangle2.width = width
        rectangle3.width = width / 2

        rectangle1.y = height * 3 / 4

        rectangle3.y = height / 4
    }
    onHeightChanged: {
        rectangle1.height = height / 4
        rectangle2.height = height / 4
        rectangle3.height = height / 2

        rectangle1.y = height * 3 / 4

        rectangle3.y = height / 4
    }

    MouseArea {     // drag mouse area
        id: mouseArea
        anchors.fill: parent
        enabled: dragEnabled
        drag{
            target: parent
            minimumX: minX
            minimumY: 0
            maximumY: 0
            maximumX: maxX
            smoothed: true

        }
        onMouseXChanged: {
            if(pressed)
                posChange(parent.x)
        }
    }

    Rectangle {
        id: rectangle1
        x: 0
        y: height * 3 / 4
        width: width
        height: height / 4
        color: color
        border.width: 0

    }

    Rectangle {
        id: rectangle2
        x: 0
        y: 0
        width: width
        height: height / 4
        color: color
        border.width: 0

    }

    Rectangle {
        id: rectangle3
        x: 0
        y: height / 4
        width: width / 2
        height: height / 2
        color: color
        border.width: 0

    }
}
