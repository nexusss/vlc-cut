import QtQuick 2.0
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4

Rectangle {
    id: item1
    width: 20
    height: 32
    color: "#00000000"

    property int value: 0
    property int maxval: -1
    property int minval: -1
    onValueChanged: label1.text = value
    signal up()
    signal down()
    Button {
        id: button1
        y: 0
        height: 10
        text: qsTr("")
        anchors.right: parent.right
        anchors.rightMargin: 0
        anchors.left: parent.left
        anchors.leftMargin: 0
        style: ButtonStyle {
                background: Rectangle {

                    color: "transparent"
                }
            }
        onClicked:{
            if(maxval === -1 || value <= maxval){
                value++
                up()
            }

        }

        Image {
            id: image1
            anchors.fill: parent
            source: "up.png"
        }
    }

    Button {
        id: button2
        y: 22
        height: 10
        text: qsTr("")
        anchors.right: parent.right
        anchors.rightMargin: 0
        anchors.left: parent.left
        anchors.leftMargin: 0
        style: ButtonStyle {
                background: Rectangle {

                    color: "transparent"
                }
            }
        onClicked: {
            if(minval === -1 || value >= minval){
                value--
                down()
            }
        }

        Image {
            id: image2
            anchors.fill: parent
            source: "down.png"
        }
    }

    Label {
        id: label1
        y: 15
        color: "#ffffff"
        text: qsTr("0")
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
        anchors.left: parent.left
        anchors.leftMargin: 0
        anchors.right: parent.right
        anchors.rightMargin: 0
        anchors.verticalCenter: parent.verticalCenter
    }



}
