
function convertValueToRect(value,width,height,maximumValue){
    var valuePos = value / maximumValue
    var xPos = (width - 15) * ( valuePos)
    var result = {resX: xPos, resY: 0,resW: (75 / 160) * height,resH: height} ;

    return result;
}

function convertXtoValue(x,width,height,maximumValue){
    var result = maximumValue * (x) / (width - 15)
    return result
}

function createBracket(type,value,maximumValue,width,height,object,color) {
    var component = Qt.createComponent(type)
                if (component.status === Component.Ready) {
                    var childRec = component.createObject(object)
                    var rect = convertValueToRect(value,width,height,maximumValue)

                    childRec.width = rect.resW
                    childRec.height = rect.resH
                    childRec.value = value
                    childRec.color = color

                    if(type === "BracketClose.qml"){
                        childRec.x = rect.resX
                    }
                    else
                        childRec.x = rect.resX + (rect.resW / 2)

                    childRec.y = rect.resY

                    return childRec;
                }

}

function changeOpenBracketValue(index,openBracket,valueStart,width,height,maxValue){

    var startBracket = openBracket.get(index).bracket

    var rectStart = convertValueToRect(valueStart,width,height,maxValue)

    startBracket.x = rectStart.resX + rectStart.resW / 2
    startBracket.y = rectStart.resY
    startBracket.width = rectStart.resW
    startBracket.height = rectStart.resH
    startBracket.value = valueStart
}

function changeCloseBracketValue(index,closeBracket,valueStop,width,height,maxValue){

    var stopBracket = closeBracket.get(index).bracket
    var rectStop = convertValueToRect(valueStop,width,height,maxValue)

    stopBracket.x = rectStop.resX
    stopBracket.y = rectStop.resY
    stopBracket.width = rectStop.resW
    stopBracket.height = rectStop.resH

    stopBracket.value = valueStop
}

function clearBracket(openBracket,closeBracket){
    var index = 0
    var bracket;
    while(openBracket.count > 0){
        bracket = openBracket.get(index).bracket
        bracket.destroy()//уничтожаем брекет
        openBracket.remove(index)
    }
    while(closeBracket.count > 0){
        bracket = closeBracket.get(index).bracket
        bracket.destroy()//уничтожаем брекет
        closeBracket.remove(index)
    }
}

function recalcPos(openBracket,closeBracket,width,maxValue){
    for(var i = 0; i < openBracket.count; i++){
        var bracket = openBracket.get(i).bracket

        var rectStop = convertValueToRect(bracket.value,width,bracket.height,maxValue)
        bracket.x = rectStop.resX
        bracket.y = rectStop.resY
        bracket.width = rectStop.resW
        bracket.height = rectStop.resH
    }

    for(var i = 0; i < closeBracket.count; i++){
        var bracket = openBracket.get(i).bracket
        var rectStop = convertValueToRect(bracket.value,width,bracket.height,maxValue)

        bracket.x = rectStop.resX
        bracket.y = rectStop.resY
        bracket.width = rectStop.resW
        bracket.height = rectStop.resH
    }
}

