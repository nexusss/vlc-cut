#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include <QObject>
#include <QVector>
#include <QQmlApplicationEngine>
#include "filelist.h"

struct SurfaceStruct{
    int x;
    int y;
    int width;
    int height;
    int angle;//угол пооврта в градусах
    qreal startTime;
    qreal stopTime;
    int type;
    struct Text{
        QString text;
        QString color;
        QString fontName;
        bool highLightText;
        QString highLightTextColor;
    }text;
    struct Img{
        QString path;
    }img;
    struct Blur{
        QString raduis;
    }blur;
    struct Music{
        QString path;
        qreal length;
        QString volume;
    }music;

};

class MainWindow : public QObject
{
    Q_OBJECT
    QObject *object;
    QVector <double> curOpenBracketVector,curCloseBracketVector,//current bracket, added from gui
    allOpenBracketVector,allCloseBracketVector;

    FileList *fileList;

    QList <SurfaceStruct> surfaces;
    bool muteMainChannel = false;


    QStringList startTime,stopTime;//start cut time, stop cut time
    void cutWithffmpeg(QString fileName);
    void mergeImageAndText(QString fileName);

public:
    MainWindow(QQmlApplicationEngine &engine, QObject *parent = 0);

signals:
     void playPl(QVariant startTime,QVariant stopTime,QVariant path);
     void successSignal();
public slots:
     void openPl(QString fileName);

     void addSurface(int x, int y,int height,int width,qreal start,qreal stop,int type);
     void setText(int index, QString text);
     void setImage(int index, QString path);
     void setMusic(int index, QString path,qreal length);

     void setMuteMainChannel(bool state);

     void updateGeomParam(int index, int x, int y, int width, int height,int rotation);
     void updateTimeParam(int index, qreal startTime, qreal stopTime);
     void updateTextParam(int index, QString text, QString color, QString fontName,bool highlightState,QString highlightColor);
     void updateRaduis(int index, int radius);
     void updateVolume(int index, int volume);
     void delSurface(int index);
     void updateIndex(int oldIndex, int newIndex);

    void addBracket(bool type, double bracket);
    void undoBracket();
    void createPl(QString filename, double length);
    void clearBracket();


    void clearImageAndText();

    void exportFile(QString fileName, double length);
};

#endif // MAINWINDOW_H
