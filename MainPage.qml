import QtQuick 2.6
import QtQuick.Controls 1.5
import QmlVlc 0.1
import QtMultimedia 5.0

import QtQuick.Dialogs 1.2
import QtQuick.Controls.Styles 1.4
import QtGraphicalEffects 1.0
import QtQuick.Layouts 1.3

import "converSeconds.js" as ConvertSeconds
import "convertJS.js" as ImageJS
import "mainPageJS.js" as Mp
import "JS/GetFileName.js" as GetFileName
import "style"

Rectangle {
    id: rectangle1
    property string videoFilePath: ""
    onVideoFilePathChanged: {
        if(videoFilePath){
            var url = "file:///" + videoFilePath
            if(url.toString().indexOf(".pl") !== -1){
                openPl(url)
                notPl = false
            }
            else{
                vlcPlayer.mrl = url
                plArray.clear()
                plArray.append({"startTime": 0, "stopTime":vlcPlayer.length / 1000.0,"newStartTime":0,"newStopTime":vlcPlayer.length / 1000.0,"cutDelta":0})
                newFile()
                notPl = true
            }


            clearImageAndText()
            playButton.enabled = false
            pauseButton.enabled = true
            stopButton.enabled = true
            progressBar1.clearBracket()
            currentPiece = 0
        }
    }

    color: "#222222"
    visible: true
    signal createPl(string filename,double length)//create play list file
    signal openPl(string filename)//open play list file
    signal exportFile(string filename,double length)//ffmpeg cut
    signal sendBracket(bool bracketType,double bracketValue)//add bracket(red)
    signal newFile();//open new file
    signal maxChange(real maxValue)//length change
    signal valueChange(real value)//progress bar value changed

    signal addSurfaceSignal(rect surfRect,rect previewRect,real startTime,real stopTime,int type) //add image
    signal delSurfaceSignal(int index)//del image
    signal updateGeomParamSignal(int index,int x,int Y,int width,int height,int rotation)
    signal updateGeomParamPreviewSignal(int index,int x,int Y,int width,int height)
    signal updateTimeParamSignal(int index, real startTime, real stopTime)
    signal updateTextParamSignal(int index,string text,string color,string fontName,bool higglightState,string highlightColor)
    signal updateIndexSignal(int oldIndex,int newIndex)
    signal updateRaduisSignal(int index,int raduis)
    signal updateVolumeSignal(int index,int raduis)
    signal setPath(int index,string path)
    signal setMusicPath(int index,string path,real length)
    signal setText(int index,string text)

    signal setMuteState(bool state)

    signal clearImageAndTextSignal()

    signal blockSurfaceInput(bool state)

    signal sendVideoSize(var width, var height)
    signal sendVideoName(string name)
    property int currentPiece: 0
    property bool notPl: true
    width: 420

    BusyIndicator {
        id: busyIndicator1
        width: 420
        visible: false
        z: 1
        anchors.fill: parent
    }

    MessageDialog {
        id: messageDialog
        text: "Success"
        Component.onCompleted: visible = false
    }

    MessageDialog {
        id: alertDialog

        text: "You can’t trim after adding extra layers, delete all the layers to trim your video"
    }


    VlcPlayer {
        id: vlcPlayer

        onMediaPlayerOpening: {
            playPause.currentIndex = 1
            //calc width height vlc video output
            var prop = vlcSurface.height / vlcPlayer.video.height
            var newWidth = vlcPlayer.video.width * prop

            if(newWidth > parent.width){
                newWidth = parent.width
                vlcSurface.width = newWidth
                var propH = vlcSurface.width / vlcPlayer.video.width
                var newHeight = vlcPlayer.video.height * propH
                vlcSurface.height = newHeight
            }
            else{
                vlcSurface.width = vlcPlayer.video.width * prop
            }
            sendVideoSize(vlcPlayer.video.width,vlcPlayer.video.height)
            sendVideoName(GetFileName.getFileName(vlcPlayer.mrl))
        }

        onPositionChanged: {
            if(plArray.count == 1)
                plArray.set(0,{"stopTime":vlcPlayer.length / 1000.0,"newStopTime":vlcPlayer.length / 1000.0})

            progressBar1.maximumValue = plArray.get(plArray.count - 1).newStopTime

            var value = (vlcPlayer.time / 1000.0) - plArray.get(currentPiece).cutDelta
            if(value > plArray.get(currentPiece).newStopTime){

                if(currentPiece + 1 < plArray.count){

                    currentPiece += 1

                    vlcPlayer.time = (value + plArray.get(currentPiece).cutDelta) * 1000

                }
            }
            progressBar1.value = value
        }
        onMediaPlayerEndReached:  {
            console.log("end reached")
            playButton.enabled = true
            playPause.currentIndex = 0
            stopMusic()
        }
    }

    VideoOutput {

        id: vlcSurface
        source: vlcPlayer;
        x: 0
        y: 0
        height: 239
        anchors.horizontalCenter: parent.horizontalCenter
        onWidthChanged: vlcTransportenSurf.width = width
        onHeightChanged: vlcTransportenSurf.height = height + 1
    }

    Rectangle{
        id: vlcTransportenSurf
        x: vlcSurface.x + 1
        y: vlcSurface.y
        height: vlcSurface.height + 1
        width: vlcSurface.width
        color: "#00000000"
    }

    SliderWithBorder {
        id: progressBar1
        objectName: "progressBar"
        y: 302
        height: 22
        anchors.left: parent.left
        anchors.leftMargin: 8
        anchors.right: parent.right
        anchors.rightMargin: 8
        onMaximumValueChanged: {

            maxChange(progressBar1.maximumValue)
            allTimeLabel.text = ConvertSeconds.toHHMMSS(progressBar1.maximumValue)
        }

        onNewBracket: {
            if(surfaceArray.count === 0){
                var piece = findPiece(bracketValue)
                sendBracket(bracketType,bracketValue + plArray.get(piece).cutDelta)
            }
            else{
                clearBracket()
                alertDialog.open()
            }
        }

        onCountBracketChanged: {
            if(countBracket === 0){
                blockSurfaceInput(false)
            }
            else
                blockSurfaceInput(true)
        }

        onValueChanged: {
            curTimeLabel.text = ConvertSeconds.toHHMMSS(value)

            checkImageTextVisible(value)

            valueChange(value)

            if (pressed == true){//rewind or forward
                changePos(value)
                if(currentPiece < 0)
                    return
                if(value < plArray.get(currentPiece).newStartTime){
                    for(var i = 0; i < currentPiece; i++){
                        if(value >= plArray.get(i).newStartTime && value < plArray.get(i).newStopTime){
                            currentPiece = i
                            vlcPlayer.time = (value + plArray.get(i).cutDelta) * 1000
                            i = currentPiece
                        }
                    }
                }
                else if(value > plArray.get(currentPiece).newStopTime){
                    for(var i = currentPiece + 1; i < plArray.count; i++){
                        if(value >= plArray.get(i).newStartTime && value <= plArray.get(i).newStopTime){
                            currentPiece = i
                            vlcPlayer.time = (value + plArray.get(i).cutDelta) * 1000
                            i = plArray.count
                        }
                    }
                }
                else{
                    vlcPlayer.time = (value + plArray.get(currentPiece).cutDelta )* 1000
                }
            }

        }

    }

    StackLayout{
        id: playPause
        x: 8
        y: 273
        width: 26
        height: 23
        currentIndex: 0

        Button {
            id: playButton


            Image {
                x: 8
                y: 4
                width: 10
                height: 15
                source: "play.png"
            }

            style: MyButtonStyle{}
            onClicked: {
                vlcPlayer.play()
                playButton.enabled = false
                pauseButton.enabled = true
                stopButton.enabled = true
                currentPiece = 0
                playMusic()
                playPause.currentIndex = 1
            }
        }

        Button {
            id: pauseButton


            enabled: false
            Image {
                anchors.bottomMargin: 4
                anchors.topMargin: 4
                anchors.rightMargin: 8
                anchors.leftMargin: 8
                anchors.fill: parent
                source: "pause.png"
            }
            onClicked: {
                vlcPlayer.pause()
                playButton.enabled = true
                pauseButton.enabled = false
                pauseMusic()
                playPause.currentIndex = 0
            }
            style: MyButtonStyle{}
        }
    }

    Button {
        id: stopButton
        x: 40
        y: 273
        width: 26
        height: 23

        Image {
            anchors.bottomMargin: 6
            anchors.topMargin: 6
            anchors.rightMargin: 8
            anchors.leftMargin: 8
            anchors.fill: parent
            source: "stop.png"
        }
        enabled: false
        onClicked: {
            vlcPlayer.stop()
            playButton.enabled = true
            stopButton.enabled = false
            stopMusic()
            playPause.currentIndex = 0
        }
        style: MyButtonStyle{}

    }

    StackLayout{
        id: muteUnmute
        x: 72
        y: 273
        width: 26
        height: 23
        currentIndex: 0

        Button {
            id: muteButton



            Image {

                anchors.fill: parent
                source: "icons/unmute.png"
            }
            onClicked: {
                vlcPlayer.mute()
                setMuteState(true)
                muteUnmute.currentIndex = 1
            }
            style: MyButtonStyle{}

        }
        Button {
            id: unmuteButton
            Image {

                anchors.fill: parent
                source: "icons/mute.png"
            }
            onClicked: {
                vlcPlayer.unMute()
                setMuteState(false)
                muteUnmute.currentIndex = 0
            }
            style: MyButtonStyle{}

        }
    }

    Rectangle {
        id: rectangle2
        x: 0
        y: 239
        width: 640
        height: 19
        color: "#000000"
    }

    Label {
        id: curTimeLabel
        x: 104
        y: 273
        width: 80
        height: 23
        text: qsTr("00:00:00.000")
        color: "white"
        verticalAlignment: Text.AlignVCenter
        font.bold: false
        font.pointSize: 10
        horizontalAlignment: Text.AlignRight
    }

    Label {
        id: label1
        x: 183
        y: 273
        width: 16
        height: 23
        text: qsTr("/")
        color: "white"
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
        font.bold: true
        font.pointSize: 10
    }



    Label {
        id: allTimeLabel
        x: 199
        y: 273
        width: 77
        height: 23
        text: qsTr("00:00:00.000")
        verticalAlignment: Text.AlignVCenter
        color: "white"
        font.bold: false
        font.pointSize: 10
        horizontalAlignment: Text.AlignLeft
    }

    Button {
        id: createM3Ufile
        x: 592
        y: 330
        width: 40
        height: 23
        text: "Trim"
        anchors.right: parent.right
        anchors.rightMargin: 8

        onClicked: {
            if(vlcPlayer.mrl != ""){
                createPl(vlcPlayer.mrl,vlcPlayer.length / 1000.0)
            }
        }
        style: MyButtonStyle{}
    }



    ListModel {
        id: plArray//trim array from pl file
        property real startTime: 0
        property real stopTime: 0
        property real newStartTime: 0
        property real newStopTime: 0
        property real cutDelta: 0
    }

    ListModel{
        id: surfaceArray
        property var surf
    }

    function updateProgressBarWidth(width){
        progressBar1.anchors.rightMargin = parent.width - (width - 4)
    }

    function addVideo(){
        if(vlcPlayer.mrl != ""){
            exportFile(vlcPlayer.mrl,vlcPlayer.length / 1000.0)
            busyIndicator1.visible = true
        }
    }

    function openPlFile(startTime,endTime,path){
        playButton.enabled = false
        pauseButton.enabled = true
        stopButton.enabled = true
        progressBar1.clearBracket()

        if(startTime.length !== endTime.length  )
            return

        if(path === ""){
            messageDialog.text = "Unsuccess"
            messageDialog.open()
        }
        else{
            plArray.clear()
            var cutDelta = 0
            for(var i = 0; i < startTime.length; i++){
                var start = startTime[i];
                var stop = endTime[i];
                if(i > 0)
                    cutDelta += startTime[i] - endTime[i - 1]

                var newStart = startTime[i] - cutDelta
                var newStop = endTime[i] - cutDelta
                plArray.append({"startTime": parseFloat(start), "stopTime": parseFloat(endTime[i]),"newStartTime": parseFloat(newStart),"newStopTime": parseFloat(newStop),"cutDelta":cutDelta})
            }


            vlcPlayer.time = plArray.get(0).startTime
            currentPiece = 0
            vlcPlayer.mrl = path
            vlcPlayer.play()
            showSuccessDialog()
        }
    }

    function showSuccessDialog(){
        messageDialog.text = "Success"
        messageDialog.open()
        busyIndicator1.visible = false
    }

    function findPiece(value){
        for(var i = 0; i < plArray.count; i++){
            if(value >= plArray.get(i).newStartTime && value <= plArray.get(i).newStopTime){
                return i;
            }
        }
    }
    //****
    function textChange(index,text,colorText,fontName,highlightState,highlightColor){

        if(index < surfaceArray.count){
            surfaceArray.get(index).surf.textstring = text
            surfaceArray.get(index).surf.textColor = colorText
            surfaceArray.get(index).surf.fontName = fontName

            surfaceArray.get(index).surf.textHighlightColor = highlightColor
            surfaceArray.get(index).surf.textHighlight = highlightState

            checkImageTextVisible(progressBar1.value)
        }
    }

    function radiusChange(index,radius){
        surfaceArray.get(index).surf.blurLevel = radius
    }

    function addText(text,startTime,stopTime,rect,angle){

        Mp.addText(surfaceArray.count,text,startTime,stopTime,rect,angle)
    }

    function addImage(path,startTime,stopTime,rect,angle){
        Mp.addImage(surfaceArray.count,path,startTime,stopTime,rect,angle)
    }
    function addBlur(startTime,stopTime,rect){
        Mp.addBlur(surfaceArray.count,startTime,stopTime,rect)
    }

    function addMusic(path,startTime,stopTime,x,y,width,height,angle){
        Mp.addMusic(surfaceArray.count,path,startTime,stopTime,x,y,width,height,angle)
    }

    function imgStartStopTimeChange(index,startTime,stopTime){
        surfaceArray.get(index).surf.startTime = startTime
        surfaceArray.get(index).surf.stopTime = stopTime

        checkImageTextVisible(progressBar1.value)

    }
    //************
    function checkImageTextVisible(value){
        for(var i = 0; i < surfaceArray.count;i++){
            var startTime = surfaceArray.get(i).surf.startTime
            var stopTime = surfaceArray.get(i).surf.stopTime
            if(value >= startTime && value <= stopTime )
                surfaceArray.get(i).surf.visible = true
            else
                surfaceArray.get(i).surf.visible = false
        }
    }

    function changePos(value){
        for(var i = 0; i < surfaceArray.count;i++){
            surfaceArray.get(i).surf.posChange(value)
        }
    }
    function playMusic(){
        for(var i = 0; i < surfaceArray.count;i++){
            surfaceArray.get(i).surf.play()
        }
    }
    function pauseMusic(){
        for(var i = 0; i < surfaceArray.count;i++){
            surfaceArray.get(i).surf.pause()
        }
    }
    function stopMusic(){
        for(var i = 0; i < surfaceArray.count;i++){
            surfaceArray.get(i).surf.stop()
        }
    }

    function updateVolumeMusic(index,volume){
        surfaceArray.get(index).surf.updateVolume(volume)
        updateVolumeSignal(index,volume)
    }

    function delSurface(index){

        surfaceArray.get(index).surf.destroy()
        delSurfaceSignal(index)
        surfaceArray.remove(index)


        for(var i = index; i < surfaceArray.count; i++)
            surfaceArray.get(i).surf.index = i
    }

    function clearImageAndText(){
        for(var i = 0; i < surfaceArray.count; i++)
            surfaceArray.get(i).surf.destroy()

        surfaceArray.clear()
        clearImageAndTextSignal()
    }

    function indexChange(oldIndex,newIndex){
        surfaceArray.get(newIndex).surf.index = oldIndex
        surfaceArray.get(oldIndex).surf.index = newIndex
        updateIndexSignal(oldIndex,newIndex)
        surfaceArray.move(oldIndex,newIndex,1)
    }
}


