import QtQuick 2.6
//import Qt.labs.controls 1.0
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.1

Item {
    height: 16
    width: 95
    property int fontSize: 10
    property real maxTime: 0
    property var colorText: "black"
    property real defaultValue: 0
    property real seconds: 0

    signal valueChanged(real value)

    onDefaultValueChanged: {
        if(defaultValue > maxTime)
            seconds = maxTime
        else
            seconds = defaultValue


        convertToHHMMSS()
    }

    onMaxTimeChanged: checkMaxTime()

    onSecondsChanged: {
        convertToHHMMSS()
    }

    RowLayout{

        spacing: 0
        anchors.fill: parent
        Rectangle{
            width: 16
            height: 16
            color: "transparent"
            Layout.minimumHeight: 15
            Layout.columnSpan: 0
            Layout.rowSpan: 0
            border.width: 0

            TextInput {
                id: hourField
                width: 16

                text: "00"
                color: colorText
                anchors.verticalCenter: parent.verticalCenter
                anchors.right: parent.right
                anchors.left: parent.left
                Layout.columnSpan: 0
                Layout.rowSpan: 0
                font.pointSize: fontSize

                horizontalAlignment: Text.AlignHCenter

                validator: RegExpValidator { regExp: /[0-9]?\d/ }
                onEditingFinished:  {
                    seconds = getSeconds()
                    checkMaxTime()
                    valueChanged(seconds)
                }
            }
            Layout.minimumWidth: 15

            Layout.fillWidth: true
            Layout.fillHeight: true
        }

        Label {
            x: 0
            width: 2
            text: ":"
            color: colorText
            Layout.rowSpan: 0
            Layout.columnSpan: 0
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            Layout.minimumWidth: 0
            Layout.fillWidth: true
            Layout.fillHeight: true
            font.pointSize: fontSize
        }

        Rectangle{
            id: rectangle1
            width: 16
            height: 16
            color: "transparent"
            Layout.columnSpan: 0
            Layout.rowSpan: 0
            Layout.minimumHeight: 15
            border.width: 0

            TextInput {
                id: minField
                width: 15
                text: "00"
                anchors.verticalCenter: parent.verticalCenter
                color: colorText
                anchors.right: parent.right
                anchors.rightMargin: 0
                anchors.left: parent.left
                anchors.leftMargin: 0
                font.pointSize: fontSize
                horizontalAlignment: Text.AlignHCenter
                validator: RegExpValidator { regExp: /[0-5]?\d/ }
                onEditingFinished:  {
                    seconds = getSeconds()
                    checkMaxTime()
                    valueChanged(seconds)
                }
            }
            Layout.minimumWidth: 15

            Layout.fillWidth: true
            Layout.fillHeight: true
        }

        Label {
            x: 0
            width: 2
            text: ":"
            color: colorText
            Layout.columnSpan: 0
            Layout.rowSpan: 0
            Layout.minimumWidth: 0
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            Layout.fillHeight: true
            Layout.fillWidth: true
            font.pointSize: fontSize
        }

        Rectangle{
            width: 15
            color: "transparent"
            Layout.minimumHeight: 15
            TextInput {
                id: secField
                width: 15
                height: 16
                text: "00"
                anchors.verticalCenterOffset: 0
                color: colorText
                anchors.left: parent.left
                anchors.leftMargin: 0
                anchors.right: parent.right
                anchors.rightMargin: 0
                anchors.verticalCenter: parent.verticalCenter
                font.pointSize: fontSize
                horizontalAlignment: Text.AlignHCenter
                validator: RegExpValidator { regExp: /[0-5]?\d/ }
                onEditingFinished:  {
                    seconds = getSeconds()
                    checkMaxTime()
                    valueChanged(seconds)
                }
            }
            Layout.columnSpan: 0
            Layout.rowSpan: 0
            Layout.minimumWidth: 15
            Layout.fillHeight: true

            Layout.fillWidth: true
        }

        Label {
            x: 0
            width: 2
            text: "."
            color: colorText
            horizontalAlignment: Text.AlignLeft
            verticalAlignment: Text.AlignVCenter
            Layout.fillHeight: true
            Layout.fillWidth: true
        }

        Rectangle {
            color: "#00000000"
            Layout.rowSpan: 0
            Layout.fillHeight: true
            TextInput {
                id: msecField
                width: 19
                color: colorText
                text: "000"
                inputMask: ""
                anchors.verticalCenter: parent.verticalCenter
                validator: RegExpValidator {
                    regExp: /[0-9]{0,2}\d/
                }
                font.pointSize: fontSize
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.rightMargin: 0
                horizontalAlignment: Text.AlignHCenter
                anchors.leftMargin: 0
                onEditingFinished:  {
                    seconds = getSeconds()
                    checkMaxTime()
                    valueChanged(seconds)
                }
            }
            Layout.minimumWidth: 16
            Layout.minimumHeight: 16
            Layout.fillWidth: true
            Layout.columnSpan: 0
        }

    }
    function getSeconds(){
        var seconds = parseInt(hourField.text) * 3600
        seconds += parseInt(minField.text) * 60
        seconds += parseInt(secField.text)
        seconds += parseInt(msecField.text) / 1000
        return Math.round(seconds * 1000) / 1000
    }

    function checkMaxTime(){
        if(seconds > maxTime){
            seconds = maxTime
            valueChanged(seconds)
        }
    }

    function convertToHHMMSS(){
        checkMaxTime()

        var hours = Math.floor(seconds / 3600);
        hourField.text = pad(hours,2)
        var minutes = Math.floor((seconds - (hours * 3600)) / 60);
        minField.text = pad(minutes,2)

        var intSec = Math.floor(seconds - (hours * 3600) - (minutes * 60))
        secField.text = pad(intSec,2)

        var msec = Math.round(seconds * 1000 - Math.floor(seconds) * 1000)
        msecField.text = pad(msec,3)
    }
    function pad(num, size) {
        var s = num+"";
        while (s.length < size) s = "0" + s;
        return s;
    }
}
