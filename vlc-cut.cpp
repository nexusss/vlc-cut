#include "vlc-cut.h"
#include <QFile>
#include <QFileInfo>
#include <QDir>
#include <QDebug>
#include <QProcess>
#include <QtConcurrent/QtConcurrentRun>
#include <QFuture>
#include <QDateTime>
#include <QFont>
#include <QPixmap>
#include <QImage>
#include <QPainter>
#include <QImageWriter>
#include <QtMath>
#include <QFile>
#include <QTextStream>
#include <QImageReader>



void unionArray(const QVector <double> &curOpenBracketVector,const QVector <double> &curCloseBracketVector,QVector <double> &allOpenBracketVector,QVector <double> &allCloseBracketVector);
void createStartStopTime(const QVector <double> &openBracketVector,const QVector <double> &closeBracketVector,const double &maxlength,QStringList &startTime,QStringList &stopTime);

VlcCutMain::VlcCutMain(QQuickView *engine, QObject *parent) : QObject(parent)
{

    QFile file("log2.txt");
    if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
              return;

          QTextStream out(&file);
          QList <QByteArray> lst = QImageReader::supportedImageFormats();
          foreach (QByteArray str, lst) {
            out << str << endl ;
          }


          file.close();
    object = engine->rootObject();


}

void VlcCutMain::init(){

    annotationPage = object->findChild<QObject*>("annotationName");
    annotationPage->setProperty("currentDir",QDir::currentPath());

    QObject *progressBar = object->findChild<QObject*>("progressBar");
    mainPage = object->findChild<QObject*>("mainPageName");
    QObject *musicPage = object->findChild<QObject*>("musicName");


    fileList = new FileList(musicPage,this);

    connect(mainPage,SIGNAL(sendBracket(bool,double)),this,SLOT(addBracket(bool,double)));
    connect(progressBar,SIGNAL(undoBracketSignal()),this,SLOT(undoBracket()));
    connect(mainPage,SIGNAL(newFile()),this,SLOT(clearBracket()));

    connect(mainPage,SIGNAL(createPl(QString,double)),this,SLOT(createPl(QString,double)));
    connect(mainPage,SIGNAL(exportFile(QString,double)),this,SLOT(exportFile(QString,double)));

    connect(mainPage,SIGNAL(openPl(QString)),this,SLOT(openPl(QString)));

    connect(mainPage,SIGNAL(maxChange(qreal)),this,SLOT(setVideoLength(qreal)));

    connect(mainPage,SIGNAL(addSurfaceSignal(QRectF,QRectF,qreal,qreal,int)),this,SLOT(addSurface(QRectF,QRectF,qreal,qreal,int)));
    connect(mainPage,SIGNAL(delSurfaceSignal(int)),this,SLOT(delSurface(int)));

    connect(mainPage,SIGNAL(setText(int,QString)),this,SLOT(setText(int,QString)));
    connect(mainPage,SIGNAL(setPath(int,QString)),this,SLOT(setImage(int,QString)));
    connect(mainPage,SIGNAL(setMusicPath(int,QString,qreal)),this,SLOT(setMusic(int,QString,qreal)));
    connect(mainPage,SIGNAL(setMuteState(bool)),this,SLOT(setMuteMainChannel(bool)));

    connect(mainPage,SIGNAL(updateGeomParamSignal(int,int,int,int,int,int)),this,SLOT(updateGeomParam(int,int,int,int,int,int)));
    connect(mainPage,SIGNAL(updateGeomParamPreviewSignal(int,int,int,int,int)),this,SLOT(updateGeomParamPreview(int,int,int,int,int)));

    connect(mainPage,SIGNAL(updateTimeParamSignal(int,qreal,qreal)),this,SLOT(updateTimeParam(int,qreal,qreal)));
    connect(mainPage,SIGNAL(updateTextParamSignal(int,QString,QString,QString,bool,QString)),this,SLOT(updateTextParam(int,QString,QString,QString,bool,QString)));
    connect(mainPage,SIGNAL(updateIndexSignal(int,int)),this,SLOT(updateIndex(int,int)));
    connect(mainPage,SIGNAL(updateRaduisSignal(int,int)),this,SLOT(updateRaduis(int,int)));
    connect(mainPage,SIGNAL(updateVolumeSignal(int,int)),this,SLOT(updateVolume(int,int)));
    connect(mainPage,SIGNAL(clearImageAndTextSignal()),this,SLOT(clearImageAndText()));

    connect(this,SIGNAL(playPl(QVariant,QVariant,QVariant)),mainPage,SLOT(openPlFile(QVariant,QVariant,QVariant)));
    connect(this,SIGNAL(successSignal()),mainPage,SLOT(showSuccessDialog()));
}

void VlcCutMain::loadNewSettings(QList <SurfaceStruct> surfaces, QString videoPath, QString videoLength, QVector <double> openBracket, QVector <double> closeBracket){
    mainPage->setProperty("videoFilePath","");
    mainPage->setProperty("videoFilePath",videoPath);
    qDebug() << "vlc cut" << videoPath << surfaces.size();
    foreach (SurfaceStruct surf, surfaces) {
        qDebug() << "type" << surf.type;
        QRect rect = QRect(surf.previewrect.x,surf.previewrect.y,
                           surf.previewrect.width,surf.previewrect.height);
        switch(surf.type){

        case 0:{
            QMetaObject::invokeMethod(annotationPage, "addImage", Qt::QueuedConnection,
                                Q_ARG(QVariant, QVariant::fromValue(surf.img.path)),
                                Q_ARG(QVariant, QVariant::fromValue(surf.startTime)),
                                Q_ARG(QVariant, QVariant::fromValue(surf.stopTime)),
                                Q_ARG(QVariant, QVariant(videoLength)),
                                Q_ARG(QVariant, QVariant(rect)),
                                Q_ARG(QVariant, QVariant(surf.angle)));
      break;
            }
        case 1:{
            QMetaObject::invokeMethod(annotationPage, "addText", Qt::QueuedConnection,
                                Q_ARG(QVariant, QVariant::fromValue(surf.startTime)),
                                Q_ARG(QVariant, QVariant::fromValue(surf.stopTime)),
                                Q_ARG(QVariant, QVariant(videoLength)),
                                Q_ARG(QVariant, QVariant::fromValue(surf.text.text)),
                                Q_ARG(QVariant, QVariant::fromValue(surf.text.color)),
                                Q_ARG(QVariant, QVariant::fromValue(surf.text.fontName)),
                                Q_ARG(QVariant, QVariant::fromValue(surf.text.highLightText)),
                                Q_ARG(QVariant, QVariant::fromValue(surf.text.highLightTextColor)),
                                Q_ARG(QVariant, QVariant(rect)),
                                Q_ARG(QVariant, QVariant(surf.angle)));
            break;
            }
        case 2:{
            QMetaObject::invokeMethod(annotationPage, "addBlur", Qt::QueuedConnection,
                                Q_ARG(QVariant, QVariant::fromValue(surf.startTime)),
                                Q_ARG(QVariant, QVariant::fromValue(surf.stopTime)),
                                Q_ARG(QVariant, QVariant(videoLength)),
                                Q_ARG(QVariant, QVariant(surf.blur.previewRadius)),
                                Q_ARG(QVariant, QVariant(rect)));

            break;
            }
        case 3:{
            QFileInfo mfi(surf.music.path);
            QMetaObject::invokeMethod(annotationPage, "addMusic", Qt::QueuedConnection,
                                Q_ARG(QVariant, QVariant::fromValue(surf.music.path)),
                                Q_ARG(QVariant, QVariant::fromValue(mfi.fileName())),
                                Q_ARG(QVariant, QVariant(surf.stopTime)),
                                Q_ARG(QVariant, QVariant::fromValue(surf.startTime)),
                                Q_ARG(QVariant, QVariant(videoLength)));
            break;
            }
        }
    }
}

void VlcCutMain::updateIndex(int oldIndex, int newIndex){
    surfaces.move(oldIndex,newIndex);
}

void VlcCutMain::setVideoLength(qreal length){
    this->videoLength = length;
}

void VlcCutMain::exportFile(QString fileName,double length){
    if(curOpenBracketVector.size() > 0 || allOpenBracketVector.size() > 0){
        if(curOpenBracketVector.size() > 0){
            if(curCloseBracketVector.size() < curOpenBracketVector.size())
                curCloseBracketVector << length;

            unionArray(curOpenBracketVector,curCloseBracketVector,allOpenBracketVector,allCloseBracketVector);

            QStringList startTimeNew, stopTimeNew;
            createStartStopTime(allOpenBracketVector,allCloseBracketVector,length,startTimeNew,stopTimeNew);
            startTime = startTimeNew;
            stopTime = stopTimeNew;
        }

        cutWithffmpeg(fileName);
    }
    else
        mergeImageAndText(fileName);
    emit successSignal();
}

void convertTextToImage(QList <SurfaceStruct> & surfaces){

    for (int i = 0; i < surfaces.size(); i++) {

        if(surfaces[i].type == 1){
            QImage img(surfaces[i].width,surfaces[i].height, QImage::Format_ARGB32_Premultiplied);//
            if(surfaces[i].text.highLightText == true)
                img.fill(surfaces[i].text.highLightTextColor);
            else
                img.fill(0);

            QPainter p(&img);

            QFont font(surfaces[i].text.fontName);
            font.setPixelSize(surfaces[i].height);

            p.setFont(font);
            QPen pen(surfaces[i].text.color);

            p.setPen(pen);
            p.drawText(0,img.height() - img.height() * 0.1,surfaces[i].text.text);
            p.end();
            QString path = QDir::tempPath() + "/" + "text_" + QString::number(i) + ".png";

            img.save(path,"PNG");
            surfaces[i].img.path = path;

        }
    }
}

void VlcCutMain::addSurface(QRectF surfRect, QRectF previewRect, qreal start, qreal stop, int type){
    SurfaceStruct ss;
    ss.x = surfRect.x();
    ss.y = surfRect.y();
    ss.height = surfRect.height();
    ss.width = surfRect.width();
    ss.previewrect.x = previewRect.x();
    ss.previewrect.y = previewRect.y();
    ss.previewrect.width = previewRect.width();
    ss.previewrect.height = previewRect.height();
    ss.startTime = start;
    ss.stopTime = stop;
    ss.type = type;
    ss.angle = 0;
    ss.blur.ffmpegRaduis = QString::number(50 *0.005);
    ss.music.volume = "1";
    ss.text.highLightText = false;
    ss.text.highLightTextColor = "#f9ee13";

    surfaces.append(ss);
}

void VlcCutMain::setText(int index, QString text){

    if(index < surfaces.size()){
        surfaces[index].text.text = text;
        surfaces[index].text.color = "black";
        surfaces[index].text.fontName = "Verdana";
    }
}

void VlcCutMain::setImage(int index, QString path){
    if(index < surfaces.size()){
        surfaces[index].img.path = path;
    }
}

void VlcCutMain::setMusic(int index, QString path, qreal length){
    if(index < surfaces.size()){
        surfaces[index].music.path = path;
        surfaces[index].music.length = length;
    }
}

void VlcCutMain::setMuteMainChannel(bool state){
    muteMainChannel = state;
}

void VlcCutMain::updateVolume(int index, int volume){
    if(index < surfaces.size()){
        surfaces[index].music.volume = QString::number((volume * 0.8) / 100.0);
    }
}

void VlcCutMain::updateRaduis(int index,int radius){
    if(index < surfaces.size()){
        surfaces[index].blur.ffmpegRaduis = QString::number(radius * 0.005);
        surfaces[index].blur.previewRadius = QString::number(radius);
    }
}

void VlcCutMain::updateGeomParam(int index, int x, int y, int width, int height,int rotation){
    if(index < surfaces.size()){
        surfaces[index].x = x;
        surfaces[index].y = y;
        surfaces[index].width = width;
        surfaces[index].height = height;
        surfaces[index].angle = rotation;
    }
}

void VlcCutMain::updateGeomParamPreview(int index, int x, int y, int width, int height){
    if(index < surfaces.size()){
        surfaces[index].previewrect.x = x;
        surfaces[index].previewrect.y = y;
        surfaces[index].previewrect.width = width;
        surfaces[index].previewrect.height = height;

    }
}

void VlcCutMain::updateTimeParam(int index, qreal startTime, qreal stopTime){
    if(index < surfaces.size()){
        surfaces[index].startTime = startTime;
        surfaces[index].stopTime = stopTime;
    }
}

void VlcCutMain::updateTextParam(int index, QString text, QString color, QString fontName, bool highlightState, QString highlightColor){
    if(index < surfaces.size()){
        surfaces[index].text.text = text;
        surfaces[index].text.color = color;
        surfaces[index].text.fontName = fontName;
        surfaces[index].text.highLightText = highlightState;
        surfaces[index].text.highLightTextColor = highlightColor;
    }
}

void VlcCutMain::clearImageAndText(){

    surfaces.clear();
}

void VlcCutMain::delSurface(int index){

    surfaces.removeAt(index);
}


QString createComplexFilterString(const QList <SurfaceStruct> imgStruct,bool muteMainChannel,QStringList &vOut,QStringList &mOut,QStringList &input){
    QString filterStr;//filter complex
    QStringList logo;

    QStringList split1,split2;
    QStringList blur;
    quint16 inputCount = 1;
    vOut << "[0:v]";
    mOut << "[0:a]";

    if(muteMainChannel){
        filterStr = mOut.last() + "volume=0[musicM];";
        mOut << "[musicM]";
    }

    QListIterator<SurfaceStruct> isIter(imgStruct);
     isIter.toBack();
     while (isIter.hasPrevious()){

        SurfaceStruct is = isIter.previous();

        if(is.type == 1 || is.type == 0){//image, arrow
            input << "-i" << is.img.path.remove("file:///");

            logo << "[logo" + QString::number(inputCount) + "]";

            QString rotate = "rotate=" + QString::number(is.angle * M_PI / 180) + ":c=none:ow=rotw(" + QString::number(is.angle * M_PI / 180)+ "):oh=roth(" + QString::number(is.angle * M_PI / 180) +")";
            QString scale = "[" + QString::number(inputCount) + ":v]scale=" + QString::number(is.width) + ":" + QString::number(is.height) + "," + rotate + logo.last() + ";";

            QString between = "\'between(t," + QString::number(is.startTime) + "," + QString::number(is.stopTime) + ")\'";
            QString overlay = "overlay=" + QString::number(is.x) + ":" + QString::number(is.y) + ":enable=" + between ;

            filterStr += scale + vOut.last() + logo.last() + overlay;
            inputCount++;

            vOut << "[out" + QString::number(vOut.size()) + "]";
            filterStr += vOut.last() + ";";
        }
        else if(is.type == 2){//blur
            split1 << "[sp1" + QString::number(split1.size()) + "]";
            split2 << "[sp2" + QString::number(split2.size()) + "]";
            QString split = vOut.last() + "split" + split1.last() + split2.last() + ";";
            blur << "[blur" + QString::number(blur.size()) + "]";
            QString cropRect = QString::number(is.width) + ":" + QString::number(is.height) + ":" + QString::number(is.x) + ":" + QString::number(is.y);
            QString crop = split1.last() + "crop=" + cropRect + ",boxblur=lr=(min(h\\,w)/2)*" + is.blur.ffmpegRaduis + ":cr=(min(cw\\,ch)/2)*" + is.blur.ffmpegRaduis + blur.last() + ";";
            QString between = "\'between(t," + QString::number(is.startTime) + "," + QString::number(is.stopTime) + ")\'";
            QString overlay = "overlay=" + QString::number(is.x) + ":" + QString::number(is.y) + ":enable=" + between ;

            filterStr += split + crop + split2.last() + blur.last() + overlay;

            vOut << "[out" + QString::number(vOut.size()) + "]";
            filterStr += vOut.last() + ";";

        }
        else if(is.type == 3){//music
            QStringList out;
            QString param ;

            qreal allLength = 0;
            qreal delayTime = is.startTime * 1000;
            if(delayTime == 0)
                delayTime = 1;

            //mute main channel

            qreal lastAudioLength = is.music.length;

            while (is.stopTime - is.startTime >= allLength + is.music.length){
                input << "-i" << is.music.path.remove("file:///");

                out << "[music" + QString::number(inputCount) + "]";

                param += "[" + QString::number(inputCount) + ":a]" + "volume=" + is.music.volume + "," + "adelay=" + QString::number(delayTime) + "|" + QString::number(delayTime) + out.last() + ";";
                inputCount++;
                allLength += is.music.length;
                delayTime += is.music.length * 1000;
            }

            if(allLength < is.stopTime - is.startTime){
                input << "-i" << is.music.path.remove("file:///");
                out << "[music" + QString::number(inputCount) + "]";
                lastAudioLength = (is.stopTime - is.startTime) - allLength;
                param += "[" + QString::number(inputCount) + ":a]atrim=0:" + QString::number(lastAudioLength) + ",volume=" + is.music.volume + "," + "adelay=" + QString::number(delayTime) + "|" + QString::number(delayTime) + out.last() + ";";
                inputCount++;
            }

            param.insert(param.indexOf(out.first()),",afade=t=in:ss=0:d=1");
            if(lastAudioLength < 1)
                lastAudioLength = 1;

            param.insert(param.indexOf(out.last()),",afade=t=out:st=" + QString::number(lastAudioLength - 1) + ":d=1");

            QString mix = mOut.last();
            foreach (QString str, out) {
                mix += str;
            }
             mix += "amix=inputs=" + QString::number(out.size() + 1);

            filterStr += param + mix;
            mOut << "[mOut" + QString::number(mOut.size()) + "]";
            filterStr += mOut.last() + ";";

        }
    }

    return filterStr;
}


void VlcCutMain::mergeImageAndText(QString fileName){
    //example ffmpeg command
//    ffmpeg.exe -i 1.avi -i play.png -i close.png  -filter_complex     "[1:v]scale=200:200[logo1];
    //    [2:v]scale=200:200[logo2];
    //    [0:v][logo1]overlay=0:0:enable='between(t,0,3)'[out1];
    //    [out1][logo2]overlay=0:0:enable='between(t,0,3)'[out2];
    //    [out2]split[sp1][sp2];
    //    [sp1]crop=200:200:60:30,boxblur=10[blur1];
    //    [sp2][blur1]overlay=60:30:enable='between(t,0,3)'"
//                                                                  -qscale 1 -y 2.avi



    if(surfaces.size() == 0)
        return;

    convertTextToImage(surfaces);

    QFileInfo fi(fileName);

    QDir dirOutput;
    dirOutput.mkdir("Output");


    QStringList list;
    list << "-i"
         << fileName.remove("file:///");

    QStringList vOut,mOut;
    QString overlay = createComplexFilterString(surfaces,muteMainChannel,vOut,mOut,list);

    overlay = overlay.remove(overlay.lastIndexOf(";"),1);

    list << "-filter_complex"
         << overlay
         << "-map";
    if(vOut.size() == 1)
        list << "0:v";
    else
        list << vOut.last();

    list << "-map";

    if(mOut.size() == 1)
        list << "0:a";
    else
        list << mOut.last();

    QDateTime timeStamp(QDateTime::currentDateTimeUtc());
    QString outputPath = dirOutput.absolutePath() + "/output/" +  QString::number(timeStamp.toMSecsSinceEpoch() / 1000) + "_"  + fi.fileName();
    list << "-q:v"
         << "1"
         << "-q:a"
         << "1"
         << "-b:a"
         << "192k"
         << "-ac"
         << "2"
         << "-y"
         <<  outputPath;


    QProcess *process = new QProcess(0);


    process->start("ffmpeg.exe",list);
    process->waitForStarted();
    process->waitForFinished(-1);


    QFile log("log.txt");
          if (!log.open(QIODevice::WriteOnly | QIODevice::Text))
              return;

          QTextStream ts(&log);
          foreach (QString s, list)
            ts << s << "\r\n";


          ts << process->readAllStandardError();
          log.close();
    delete process;
    emit videoAdded(outputPath);
}

void VlcCutMain::addBracket(bool type, double bracket){

    if(type){
        curCloseBracketVector.push_back(bracket);
    }
    else{
        curOpenBracketVector.push_back(bracket);
    }
}


void VlcCutMain::undoBracket(){
    curOpenBracketVector.pop_back();
    if(curOpenBracketVector.size() < curCloseBracketVector.size())
        curCloseBracketVector.pop_back();

}
void VlcCutMain::clearBracket(){
    curOpenBracketVector.clear();
    curCloseBracketVector.clear();
    allOpenBracketVector.clear();
    allCloseBracketVector.clear();
}

void VlcCutMain::openPl(QString fileName){
    QFileInfo fi(fileName);

    QFile plFile(fileName.remove("file:///"));
    if (!plFile.open(QIODevice::ReadOnly | QIODevice::Text)){
        emit playPl("","","");
        return;
    }

    QTextStream in(&plFile);

    QString path;
    QString line = in.readAll();
    plFile.close();

    QStringList lines = line.split("\n");
    double length = 0;
    foreach (QString str, lines) {
        QStringList list = str.split('=');
            if(list.size() > 1){
                if(str.contains("start-Cut"))
                    allOpenBracketVector << list.at(1).toDouble();

                if(str.contains("stop-Cut"))
                    allCloseBracketVector << list.at(1).toDouble();

                if(str.contains("length"))
                    length = list.at(1).toDouble();

                if(str.contains("path"))
                    path = list.at(1);
            }
    }

    QStringList startTimeNew,stopTimeNew;

    if(allCloseBracketVector.size() < allOpenBracketVector.size())
        allCloseBracketVector << length;

    if(allCloseBracketVector.size() == allOpenBracketVector.size()){
        createStartStopTime(allOpenBracketVector,allCloseBracketVector,length,startTimeNew,stopTimeNew);
        emit playPl(startTimeNew,stopTimeNew,fi.path() + "/" + path);
    }

    startTime = startTimeNew;
    stopTime = stopTimeNew;
}

inline bool copy(QString input,QString output){
    if(!QFile::exists(output.remove("file:///")))
        if(!QFile::copy(input.remove("file:///"),output.remove("file:///")))
            return false;

    return true;
}

void unionArray(const QVector <double> &curOpenBracketVector,const QVector <double> &curCloseBracketVector,QVector <double> &allOpenBracketVector,QVector <double> &allCloseBracketVector){

    allOpenBracketVector.append(curOpenBracketVector);
    allCloseBracketVector.append(curCloseBracketVector);


    qSort(allOpenBracketVector.begin(),allOpenBracketVector.end());
    qSort(allCloseBracketVector.begin(),allCloseBracketVector.end());

    for(int i = 1 ; i < allOpenBracketVector.size();i++){

        while(allOpenBracketVector.at(i) < allCloseBracketVector.at(i - 1)){
            allCloseBracketVector[i - 1] = allCloseBracketVector[i];
            allOpenBracketVector.removeAt(i);
            allCloseBracketVector.removeAt(i);
            if(i == allCloseBracketVector.size())
                break;
        }
    }
}

void createStartStopTime(const QVector <double> &openBracketVector,const QVector <double> &closeBracketVector,const double &maxlength,QStringList &startTime,QStringList &stopTime){
    for(int i = -1; i < closeBracketVector.size(); i++){
        //выставаить начальное время участка(уже приведенное к новому прогрес бару)
        if(i < 0)
            startTime << "0";
        else
            startTime << QString::number(closeBracketVector.at(i));
        //выставить конец участка
        if(i + 1 < openBracketVector.size())
            stopTime << QString::number(openBracketVector.at(i + 1));
        else
            stopTime << QString::number(maxlength);
    }
}

bool writeToFile(const QString &fileName,const QString &path,const double &length,const QVector <double> &openBracketVector,const QVector <double> &closeBracketVector){
    QFile file(fileName);
    if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
        return false;

    QTextStream out(&file);
    for(int i = 0; i < openBracketVector.size(); i++){
        out << "start-Cut=" << openBracketVector.at(i) << "\n";

        if(i < closeBracketVector.size())
            out << "stop-Cut=" << closeBracketVector.at(i) << "\n";
    }
    out << "length=" <<  length << "\n";
    out << "path=" << path;

    file.close();
    return true;
}

void VlcCutMain::createPl(QString filename,double length){

    if(curOpenBracketVector.size() == 0)
        return;

    QFileInfo fi(filename);

    QDir dirInput,dirOutput;
    dirInput.mkdir("Input");
    dirInput.cd("Input");
    dirOutput.mkdir("Output");
    dirOutput.cd("Output");

    QStringList startTimeNew,stopTimeNew;
    QString newFilePath = dirInput.absolutePath() + "/" + fi.fileName();
    copy(filename,newFilePath);   

    if(curCloseBracketVector.size() < curOpenBracketVector.size())
        curCloseBracketVector << length;

    unionArray(curOpenBracketVector,curCloseBracketVector,allOpenBracketVector,allCloseBracketVector);
    curOpenBracketVector.clear();
    curCloseBracketVector.clear();

    createStartStopTime(allOpenBracketVector,allCloseBracketVector,length,startTimeNew,stopTimeNew);

    QString name = dirOutput.absolutePath() + "/" +  fi.baseName() + ".pl";
    if(!writeToFile(name,"../Input/" + fi.fileName(),length,allOpenBracketVector,allCloseBracketVector)){
        emit playPl("","","");
        return;
    }

    startTime = startTimeNew;
    stopTime = stopTimeNew;

    emit playPl(startTimeNew,stopTimeNew,"file:///" + dirOutput.absolutePath() + "/" + "../Input/" + fi.fileName());
}

void cut(QString input,QString startTime,QString duration,QString output){
    input = input.remove("file:///");
    QProcess *process = new QProcess(0);
    QStringList str;
    str << "-i"
        << input
        << "-ss"
        << startTime
        << "-c"
        << "copy"
        << "-t"
        << duration
        << "-y"
        << output;
    process->start("ffmpeg.exe",str);
    process->waitForStarted();
    process->waitForFinished();
    delete process;
}

void merge(QString input,QString output){
    QProcess *process = new QProcess(0);
    QString concat = input + "/mylist.txt";


    QStringList str;
    str << "-f"
        << "concat"
        << "-i"
        << concat
        << "-c"
        << "copy"
        << "-y"
        << output;
    process->start("ffmpeg.exe",str);
    process->waitForStarted();
    process->waitForFinished();
    delete process;
}

void VlcCutMain::cutWithffmpeg(QString fileName){

    if(startTime.size() == 0)
        return;

    QVector <QFuture<void> > cutProc;

    QDir cutDir;
    cutDir.mkdir("Cut");
    cutDir.cd("Cut");



    QStringList outputNames,cutNames;
    QFileInfo fi(fileName);


    //cut file on picies

    for(int i = 0; i < startTime.size(); i++){
        double delta = stopTime.at(i).toDouble() - startTime.at(i).toDouble();
        outputNames << cutDir.absolutePath() + "/tmp_" + QString::number(i) + "." + fi.suffix();
        cutNames << "tmp_" + QString::number(i) + "." + fi.suffix();
        cutProc << QtConcurrent::run(cut,QString(fileName),QString(startTime.at(i)),QString(QString::number(delta)),QString(outputNames.last()));

    }
    //wait while it cutting
    foreach (QFuture<void> proc, cutProc)
        proc.waitForFinished();

    //create mylist.txt, file which need concat
    QDir mergeDir;
    QFile file("Cut/myList.txt");
    if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
              return;

    QTextStream out(&file);
    foreach (QString str, cutNames) {
        out << "file" << "\t" << "'" + str + "'"<< "\n";
    }
    file.close();

    mergeDir.mkdir("Output");
    mergeDir.cd("Output");

    //merge file
    QString mergeName = mergeDir.absolutePath() + "/" + fi.fileName();
    merge(cutDir.absolutePath(),mergeName);
    cutDir.removeRecursively();
}
