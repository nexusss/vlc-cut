
function addText(model,text,startValue,stopValue,maxStopTime){

    model.append({"imagePath":"",
                     "fullimagePath":"",
                     "newPathToMusic":"",
                     "newMusicIndex" : 0,
                     "newType":1,
                     "newArrayIndex":model.count,
                     "newtext":text,
                     "newMaxStopTime":parseFloat(maxStopTime),
                     "newStartTime" : parseFloat(startValue),
                     "newStopTime" : parseFloat(stopValue)})

}

function addBlur(model,startValue,stopValue,maxStopTime){
    model.append({"imagePath":"",
                     "fullimagePath":"",
                     "newPathToMusic":"",
                     "newMusicIndex" : 0,
                     "newType":2,
                     "newArrayIndex":model.count,
                     "newtext":"",
                     "newMaxStopTime":parseFloat(maxStopTime),
                     "newStartTime" : parseFloat(startValue),
                     "newStopTime" : parseFloat(stopValue)})

}

function addImage(model,path,startValue,stopValue,maxStopTime) {

    if (path) {
        var startIndex = (path.toString().indexOf('\\') >= 0 ? path.toString().lastIndexOf('\\') : path.toString().lastIndexOf('/'));
        var filename = path.toString().substring(startIndex);
        if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
            filename = filename.substring(1);
        }        

        model.append({"fullimagePath":path.toString(),
                         "imagePath":filename,
                         "newPathToMusic":"",
                         "newMusicIndex" : 0,
                         "newType":0,
                         "newArrayIndex":model.count,
                         "newtext":"",
                         "newMaxStopTime":parseFloat(maxStopTime),
                         "newStartTime" : parseFloat(startValue),
                         "newStopTime" : parseFloat(stopValue)})
    }
}

function addMusic(model,path,musicIndex,startValue,stopValue,maxStopTime){
    if (path) {
        model.append({"imagePath":"",
                         "fullimagePath":"",
                         "newPathToMusic":path.toString(),
                         "newMusicIndex" : musicIndex,
                         "newType":3,
                         "newArrayIndex":model.count,
                         "newtext":"",
                         "newMaxStopTime":parseFloat(maxStopTime),
                         "newStartTime" : parseFloat(startValue),
                         "newStopTime" : parseFloat(stopValue)})
    }
}



function delIndex(model,arrayIndex){

    model.remove(arrayIndex)

    for(var i = 0; i < model.count; i++){
        model.set(i,{"newArrayIndex":i})
        var getmodel = model.get(i)

    }

}
