#ifndef FILELIST_H
#define FILELIST_H

#include <QObject>
#include <QVariant>

class FileList : public QObject
{
    Q_OBJECT
    QStringList filesList;
    QObject *qmlFile;
public:
    FileList(QObject *qmlFile,QObject *parent = 0);

};

#endif // FILELIST_H
