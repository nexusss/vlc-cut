import QtQuick 2.0
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.1

import "annotationJS.js" as Annotation
import "style"

Rectangle {
    id: rectangle1
    visible: true
    color: "#222222"
    property real maxStopTime: 0
    property real currentValue: 0

    property string currentDir: ""

    signal addImageSignal(string path,real startTime,real stopTime,var rect,int angle)
    signal addTextSignal(string text,real startTime,real stopTime,var rect,int angle)
    signal addBlurSignal(real startTime,real stopTime,var rect)
    signal addMusicSignal(string path,real startTime,real stopTime)

    signal delSurfaceSignal(int index)
    signal updateTimeSignal(int index,real startTime,real stopTime)
    signal updateVolumeSignal(int index,int level)

    signal updateTextSignal(int index,string text, var colorText,string fontName,bool highlightState,string highlightColor)
    signal updateRadiusSignal(int index,int radius)
    signal indexChange(int oldIndex,int newIndex)
    signal remMusicFromPlayList(int index)
    signal updateSurfIndexInPlayList(int musicIndex,int arrayIndex)

    signal addVideo()
    signal timeBarWidthChange(int width)

    onMaxStopTimeChanged: {

        timeBar.maxValue = maxStopTime

        for(var i = 0; i < imageInputModel.count; i++)
            imageInputModel.set(i,{"stopTime":maxStopTime})

        if(maxStopTime > 0){
            addTextButton.enabled = true
            addImageButton.enabled = true
            addArrowButton.enabled = true
            addBlurButton.enabled = true
        }
        else{
            addTextButton.enabled = false
            addImageButton.enabled = false
            addArrowButton.enabled = false
            addBlurButton.enabled = false
        }

    }

    FileDialog {
        id: imageDialog
        visible: false
        title: "open image"
        nameFilters: [ "Image files (*.jpg *.jpeg *.png)" ]
        onAccepted: {
            addImage(imageDialog.fileUrl)
        }
    }

    TimeBar {
        id: timeBar
        y: 0

        width: imageListView.width
        maxValue: 0
        onWidthChanged: {
            timeBarWidthChange(width)
        }
    }

    ScrollView {
        id: imageScrol
        x: 0
        y: timeBar.height
        width: parent.width
        height: parent.height  - addImageButton.height - timeBar.height
        frameVisible: true

        style: ScrollViewStyle {

            handle: Item {

                implicitWidth: 24
                implicitHeight: 26
                Rectangle {
                    x: 10
                    width: 14
                    height: parent.height
                    color: "#424246"

                }
            }
            scrollBarBackground: Item {

                implicitWidth: 24
                implicitHeight: 26
            }
            decrementControl: Rectangle {
                    x:10
                        implicitWidth: 24
                        implicitHeight: 15
                        color: "gray"
                        Image {

                            width: 14
                            height: parent.height
                            source: "up.png"
                        }
                    }
                    incrementControl: Rectangle {
                        x: 10
                        implicitWidth: 24
                        implicitHeight: 15
                        color: "gray"
                        Image {

                            width: 14
                            height: parent.height
                            source: "down.png"

                        }
                    }
        }
        ListView{
            id: imageListView
            anchors.fill: parent

            model:     ListModel {
                id: imageInputModel
            }
            onWidthChanged: timeBar.width = width
            delegate: SurfaceInput{
                id: imgIn
                width: imageListView.width
                arrayIndex: newArrayIndex

                maxStopTime: newMaxStopTime
                startTimeDefault: newStartTime
                stopTimeDefault: newStopTime
                pathToImage: imagePath
                fullpathToImage: fullimagePath
                pathToMusic: newPathToMusic
                musicIndex: newMusicIndex

                onClose: {
                    delIndex(arrayIndex)
                    delSurfaceSignal(arrayIndex)

                }
                onUpdateTime: {
                    updateTimeSignal(arrayIndex,startTime,stopTime)
                }
                text: newtext
                type: newType

                onUpdateTextSurface: updateTextSignal(arrayIndex,text,colorText,fontName,highlightState,highlightColor)
                onUpdateRadius: updateRadiusSignal(arrayIndex, radius)
                onUpdateVolumeLevel: updateVolumeSignal(arrayIndex,level)

                onDownSignal: {
                    if (model.index < imageInputModel.count -1) {
                        imageInputModel.set(model.index + 1,{"newArrayIndex" : arrayIndex})
                        imageInputModel.set(model.index ,{"newArrayIndex" : arrayIndex + 1})
                        indexChange(model.index,model.index + 1)
                        imageInputModel.move (model.index, model.index +1, 1);
                    }
                }
                onUpSignal: {
                    if (model.index > 0) {
                        imageInputModel.set(model.index - 1,{"newArrayIndex" : arrayIndex})
                        imageInputModel.set(model.index,{"newArrayIndex" : arrayIndex - 1})
                        indexChange(model.index,model.index - 1)
                        imageInputModel.move (model.index, model.index -1, 1);
                    }
                }
                Component.onDestruction:  {

                    if(type === 3)
                        remMusicFromPlayList(musicIndex)
                }
            }
        }

    }

    Row {
        id: row1
        x: 0
        y: parent.height - 23
        width: 394
        height: 23
        spacing: 5
        anchors.horizontalCenter: parent.horizontalCenter

        Button {
            id: addBlurButton
            width: 75
            height: 23
            enabled: false
            text: qsTr("Add blur")
            style: MyButtonStyle{}
            onClicked: {

                addBlur()
            }
        }

        Button {
            id: addTextButton
            width: 75
            height: 23

            enabled: false
            text: qsTr("Add text")
            style: MyButtonStyle{}
            onClicked: addText()
        }

        Button {
            id: addImageButton
            width: 75
            height: 23

            enabled: false
            text: qsTr("Add image")
            style:MyButtonStyle{}
            onClicked: imageDialog.open()
        }

        Button {
            id: addArrowButton
            width: 75
            height: 23
            text: qsTr("Add arrow")
            enabled: false
            style: MyButtonStyle{}
            onClicked: {
                addImage("file:///" + currentDir + "/Arrow/arrow.png")
            }
        }

        Button {
            id: addVideoButton
            width: 75
            height: 23
            text: qsTr("Add Video")
            enabled: true
            style: MyButtonStyle {
            }
            onClicked: addVideo()
        }
    }


    function addText(startTime,stopTime,maxStopTime,text,colorText,fontName,highlightState,highlightColor,rect,angle){
        if(startTime === undefined) startTime = currentValue
        if(stopTime === undefined) stopTime = currentValue + 10
        if(maxStopTime === undefined) maxStopTime = rectangle1.maxStopTime
        if(text === undefined) text = "Add Text"
        if(colorText === undefined) colorText = "Black"
        if(fontName === undefined) fontName = "Verdana"
        if(highlightState === undefined) highlightState = false
        if(highlightColor === undefined) highlightColor = "#f9ee13"
        if(rect === undefined) rect = Qt.rect(0,0,50,50)
        if(angle === undefined) angle = 0

        var newStopTime = maxStopTime > stopTime ? stopTime : maxStopTime

        Annotation.addText(imageInputModel,text,startTime,newStopTime,maxStopTime)
        imageListView.currentIndex = imageListView.count - 1
        addTextSignal(text,startTime,newStopTime,rect,angle)

        imageListView.currentItem.setText(text,colorText,fontName,highlightState,highlightColor)
    }

    function addImage(path,startTime,stopTime,maxStopTime,rect,angle){
        if (path) {
            if (startTime === undefined) startTime = currentValue
            if(stopTime === undefined) stopTime = currentValue + 10
            if(maxStopTime === undefined) maxStopTime = rectangle1.maxStopTime
            if(rect === undefined) rect = Qt.rect(0,0,50,50)
            if(angle === undefined) angle = 0

            var newStopTime = maxStopTime > stopTime ? stopTime : maxStopTime

            Annotation.addImage(imageInputModel,path,startTime,newStopTime,maxStopTime)
            imageListView.currentIndex = imageListView.count - 1
            addImageSignal(path,startTime,newStopTime,rect,angle)
        }
    }

    function addBlur(startTime,stopTime,maxStopTime,radius,rect){
        if (startTime === undefined) startTime = currentValue
        if(stopTime === undefined) stopTime = currentValue + 10
        if(maxStopTime === undefined) maxStopTime = rectangle1.maxStopTime
        if(radius === undefined) radius = 50
        if(rect === undefined) rect = Qt.rect(0,0,50,50)


        var newStopTime = maxStopTime > stopTime ? stopTime : maxStopTime

        Annotation.addBlur(imageInputModel,startTime,newStopTime,maxStopTime)
        imageListView.currentIndex = imageListView.count - 1
        addBlurSignal(startTime,newStopTime,rect)

        imageListView.currentItem.setRadius(radius)
    }

    function addMusic(path,fileName,musicIndex,length,startTime,maxStopTime){
        if (path) {
            if (startTime === undefined) startTime = currentValue
            if(maxStopTime === undefined) maxStopTime = rectangle1.maxStopTime

            var newStopTime = maxStopTime > length ? length : maxStopTime

            Annotation.addMusic(imageInputModel,fileName,musicIndex,startTime,newStopTime, maxStopTime)
            imageListView.currentIndex = imageListView.count - 1
            addMusicSignal(path,startTime,maxStopTime)
            updateSurfIndexInPlayList(musicIndex,imageListView.currentIndex)
        }
    }

    function remMusicFromSurfInput(surfIndex){
        delIndex(surfIndex)
        delSurfaceSignal(surfIndex)
    }

    function delIndex(arrayIndex){
        Annotation.delIndex(imageInputModel,arrayIndex)
    }

    function clearImageAndText(){
        imageInputModel.clear()

    }

    function blockSurfacenInput(state){
        addArrowButton.enabled = !state
        addBlurButton.enabled = !state
        addTextButton.enabled = !state
        addImageButton.enabled = !state
    }
}

