#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QmlVlc.h>
#include <QmlVlc/QmlVlcConfig.h>
#include <QThread>
#include "mainwindow.h"
#include <QtPlugin>



int main(int argc, char *argv[])
{

    RegisterQmlVlc();
    QmlVlcConfig& config = QmlVlcConfig::instance();
    config.enableAdjustFilter( true );
    config.enableMarqueeFilter( true );
    config.enableLogoFilter( true );
    config.enableDebug( false );


    QGuiApplication app(argc, argv);

    QStringList paths ;
paths << QCoreApplication::applicationDirPath();
//       paths.append(".");
//       paths.append(QCoreApplication::applicationDirPath() + "/imageformats");
//       paths.append(QCoreApplication::applicationDirPath() + "/platforms");
//       paths.append(QCoreApplication::applicationDirPath() + "/sqldrivers");

       QCoreApplication::setLibraryPaths(paths);

    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    QThread mainThread;

    MainWindow mw(engine);
    mw.moveToThread(&mainThread);
    mainThread.start();
    return app.exec();
}
