function calcNewPoint(x,y,originalX,originalY,angle){ //calc x pos after rotation

    var sin =  Math.sin((angle) * Math.PI / 180)
    var cos = Math.cos((angle) * Math.PI / 180)

    var rotX = (x) * cos - (y) * sin
    var rotY = (x) * sin + (y) * cos


    var ret = {x: originalX + (rotX - x),y: originalY + (rotY - y)}
    return ret;
}

function calcSquarePoint(width,height,centerX,centerY){
    var pointLU = {x: centerX - width / 2, y: centerY - height / 2}
    var pointRU = {x: centerX + width / 2, y: centerY - height / 2}
    var pointLD = {x: centerX + width / 2, y: centerY + height / 2}
    var pointRD = {x: centerX - width / 2, y: centerY + height / 2}
    return [pointLU,pointRU,pointLD,pointRD]
}

function calcRotSquarePoint(x,y,width,height,angle){
    var point = calcSquarePoint(width,height,0,0)
    var origPoint = calcSquarePoint(width,height,x + width / 2,y + height / 2)


    var rotPointLU = calcNewPoint(point[0].x,point[0].y,origPoint[0].x,origPoint[0].y,angle)
    var rotPointRU = calcNewPoint(point[1].x,point[1].y,origPoint[1].x,origPoint[1].y,angle)
    var rotPointLD = calcNewPoint(point[2].x,point[2].y,origPoint[2].x,origPoint[2].y,angle)
    var rotPointRD = calcNewPoint(point[3].x,point[3].y,origPoint[3].x,origPoint[3].y,angle)

    return [rotPointLU,rotPointRU,rotPointLD,rotPointRD]
}

function calcNewMaxXY(x,y,width,height,angle){ //calc y pos after rotation
    
    var deltaSquareP = calcRotSquarePoint(x,y,width,height,angle)

    var maxX,maxY

    deltaSquareP.forEach(function(item,i){
        if(i === 0){
            maxX = item.x
            maxY = item.y

        }

        if(item.x < maxX){
            maxX = item.x
        }

        if(item.y < maxY){
            maxY = item.y
        }
    })

    return {maxX:maxX,maxY:maxY}
}
