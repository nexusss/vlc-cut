import QtQuick 2.0
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import "createBracket.js" as BracketJS
import "converSeconds.js" as ConvertSeconds
import "style"
Slider {
        id: sliderHorizontal1
//        width: 296

        property int countBracket:0
        maximumValue: 0
        signal newBracket(bool bracketType,double bracketValue)
        signal undoBracketSignal()


        Label {
                    id: startTimeLabel1
                    x: 34
                    y: 28
                    width: 79
                    height: 23
                    text: qsTr("00:00:00.000")
                    verticalAlignment: Text.AlignVCenter
                    color: "white"
                    font.bold: false
                    font.pointSize: 10
                    horizontalAlignment: Text.AlignLeft
                }


        Button {
            id: buttonOpenBracket
            x: 285
            y: 28
            width: 29
            height: 23
            text: qsTr("[")
            onClicked: setOpenBracket()
            style: MyButtonStyle{}
        }

        Button {
            id: buttonCloseBracket
            x: 319
            y: 28
            width: 29
            height: 23
            enabled: false
            text: qsTr("]")
            onClicked: setCloseBracket()
            style: MyButtonStyle{}
        }

        Button {
            id: undoButton
            x: 339
            y: -30
            width: 29
            height: 23
            Image {
                    anchors.fill: parent
                    source: "undo.png"
                }
            enabled: false
            onClicked: undoBracket()
            style: MyButtonStyle{}
        }

        Button {
            id: redoButton
            x: 374
            y: -30
            width: 29
            height: 23
            Image {
                    anchors.fill: parent
                    source: "redo.png"
                }
            enabled: false
            onClicked: redoBracket()
            style: MyButtonStyle{}
        }

        Label {
            id: label1
            x: 222
            y: 28
            width: 39
            height: 23
            text: qsTr("Trims:")
            font.pointSize: 10
            color: "white"
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }

        Label {
            id: trimCounterLabel
            x: 262
            y: 28
            width: 17
            height: 23
            text: qsTr("0")
            font.pointSize: 10
            color: "white"
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }

        ListModel {
            id: openBracketArray
            property var bracket: 0
            onCountChanged: countBracket = count
        }

        ListModel {
            id: closeBracketArray
            property var bracket: 0
        }



        ListModel {
            id: openBracketArrayUndo
            property var bracketValue: 0
        }

        ListModel {
            id: closeBracketArrayUndo
            property var bracketValue: 0

        }

        Label {
                    id: startTimeLabel
                    x: 0
                    y: 28
                    width: 28
                    height: 23
                    color: "#ffffff"
                    text: qsTr("Start")
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignLeft
                    font.pointSize: 10
                    font.bold: false
                }

                Label {
                    id: endTimeLabel1
                    x: 136
                    y: 28
                    width: 80
                    height: 23
                    color: "#ffffff"
                    text: qsTr("00:00:00.000")
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignLeft
                    font.pointSize: 10
                    font.bold: false
                }

                Label {
                    id: endTimeLabel
                    x: 113
                    y: 28
                    width: 23
                    height: 23
                    color: "#ffffff"
                    text: qsTr("End")
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignLeft
                    font.pointSize: 10
                    font.bold: false
                }

        function setOpenBracket(){
            var valueOpen = sliderHorizontal1.value

            if(sliderHorizontal1.maximumValue == 0)
                return

            for(var i = 0; i < openBracketArray.count; i++){
                var bracketPrevOpen = openBracketArray.get(i).bracket.value
                var bracketPrevClose = closeBracketArray.get(i).bracket.value
                if(bracketPrevOpen < valueOpen && bracketPrevClose > valueOpen){
                    return
                }
            }

            openBracketArray.append({"bracket":BracketJS.createBracket("BracketOpen.qml",
                                                    sliderHorizontal1.value,
                                                    sliderHorizontal1.maximumValue,
                                                    sliderHorizontal1.width,
                                                    sliderHorizontal1.height,
                                                    sliderHorizontal1,"red")})


            buttonCloseBracket.enabled = true
            buttonOpenBracket.enabled = false
            undoButton.enabled = true


            newBracket(0,sliderHorizontal1.value)
            startTimeLabel1.text = ConvertSeconds.toHHMMSS(sliderHorizontal1.value)
        }

        function setCloseBracket(){
            var valueClose = sliderHorizontal1.value
            var valueOpen = openBracketArray.get(openBracketArray.count - 1).bracket.value

            if(valueClose > valueOpen){

                closeBracketArray.append({"bracket":BracketJS.createBracket("BracketClose.qml",
                                                    sliderHorizontal1.value,
                                                    sliderHorizontal1.maximumValue,
                                                    sliderHorizontal1.width,
                                                    sliderHorizontal1.height,
                                                    sliderHorizontal1,"red")})


                buttonCloseBracket.enabled = false
                buttonOpenBracket.enabled = true

                newBracket(1,sliderHorizontal1.value)
                trimCounterLabel.text = closeBracketArray.count
                endTimeLabel1.text = ConvertSeconds.toHHMMSS(sliderHorizontal1.value)
            }
        }

        function undoBracket(){

            redoButton.enabled = true

            var index = openBracketArray.count - 1
            var bracket = openBracketArray.get(index).bracket
            openBracketArrayUndo.append({"bracketValue":bracket.value})//запоминаем удаленное
            bracket.destroy()//уничтожаем брекет
            openBracketArray.remove(index)
            if(openBracketArray.count < closeBracketArray.count){
                index = closeBracketArray.count - 1
                bracket = closeBracketArray.get(index).bracket
                closeBracketArrayUndo.append({"bracketValue":bracket.value})//запоминаем удаленное
                bracket.destroy()//уничтожаем брекет
                closeBracketArray.remove(index)
                trimCounterLabel.text = closeBracketArray.count
            }

            undoBracketSignal()
            buttonCloseBracket.enabled = false
            buttonOpenBracket.enabled = true

        }

        function redoBracket(){
            if(openBracketArrayUndo.count > 0){
                var index = openBracketArrayUndo.count - 1
                var value = openBracketArrayUndo.get(index).bracketValue;
                openBracketArray.append({"bracket":BracketJS.createBracket("BracketOpen.qml",
                                                        value,
                                                        sliderHorizontal1.maximumValue,
                                                        sliderHorizontal1.width,
                                                        sliderHorizontal1.height,
                                                        sliderHorizontal1,"red")})
                openBracketArrayUndo.remove(index)

                newBracket(0,value)

                buttonCloseBracket.enabled = true

                if(closeBracketArrayUndo.count > openBracketArrayUndo.count){
                    index = closeBracketArrayUndo.count - 1
                    value = closeBracketArrayUndo.get(index).bracketValue


                    closeBracketArray.append({"bracket":BracketJS.createBracket("BracketClose.qml",
                                                            value,
                                                            sliderHorizontal1.maximumValue,
                                                            sliderHorizontal1.width,
                                                            sliderHorizontal1.height,
                                                            sliderHorizontal1,"red")})
                    closeBracketArrayUndo.remove(index)
                    trimCounterLabel.text = closeBracketArray.count
                    newBracket(1,value)
                }
            }

            if(openBracketArrayUndo.count == 0){
               undoButton.enabled = true
               redoButton.enabled = false
            }

        }

        function clearBracket(){


            BracketJS.clearBracket(openBracketArray,closeBracketArray)
            startTimeLabel1.text = "00:00:00.000"
            endTimeLabel1.text = "00:00:00.000"
            trimCounterLabel.text = "0"
            buttonOpenBracket.enabled = true
            buttonCloseBracket.enabled = true

        }
}

