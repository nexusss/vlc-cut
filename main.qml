import QtQuick 2.6
import QtQuick.Controls 2.0
import QtQuick.Controls.Styles 1.4
import QtQuick.Layouts 1.0
//import Qt.labs.controls 1.0


Rectangle {
    visible: true
    width: 420
    height:541

    color: "#222222"

    MainPage{
        id: mainPage
        objectName: "mainPageName"
        height: 360
        width: parent.width
        anchors.rightMargin: 0
        anchors.bottomMargin: 0
        anchors.leftMargin: 0
        anchors.topMargin: 0
        onMaxChange: ii.maxStopTime = maxValue
        onValueChange: ii.currentValue = value
        onClearImageAndTextSignal: ii.clearImageAndText()
        onNotPlChanged: annotation.enabled = notPl
        onBlockSurfaceInput: ii.blockSurfacenInput(state)

    }

    SwipeView {
        id: swipeView
        y: 360
        x:0
        width: parent.width
        height: 160
        currentIndex: tabBar.currentIndex


       AnnotationPage{
                id: ii
                x: 0
                y: 0
                height: parent.height
                width: 420
                objectName: "annotationName"

                onAddImageSignal: mainPage.addImage(path,startTime,stopTime,rect,angle)
                onAddTextSignal: mainPage.addText(text,startTime,stopTime,rect,angle)
                onAddBlurSignal: mainPage.addBlur(startTime,stopTime,rect)
                onAddMusicSignal: mainPage.addMusic(path,startTime,stopTime)
                onDelSurfaceSignal: mainPage.delSurface(index)
                onUpdateTextSignal: mainPage.textChange(index,text,colorText,fontName,highlightState,highlightColor)
                onUpdateTimeSignal: mainPage.imgStartStopTimeChange(index,startTime,stopTime)
                onIndexChange: mainPage.indexChange(oldIndex,newIndex)
                onUpdateRadiusSignal: mainPage.radiusChange(index,radius)
                onUpdateVolumeSignal: mainPage.updateVolumeMusic(index,level)

                onRemMusicFromPlayList: musicPage.removeMusic(index)
                onUpdateSurfIndexInPlayList: musicPage.setSurfIndex(musicIndex,arrayIndex)

                onAddVideo: mainPage.addVideo()
                onTimeBarWidthChange: mainPage.updateProgressBarWidth(width)
        }

       MusicPage {
           id: musicPage
           objectName: "musicName"
           x: 420
           y: 0
           height: parent.height
           width: 420
           onAddMusicSignal: ii.addMusic(path,fileName,musicIndex,length)
           onRemMusicSignal: ii.remMusicFromSurfInput(surfIndex)
       }
    }

    TabBar {
        x:0
        y:520
        height: 20
        width: parent.width
        id: tabBar
        currentIndex: swipeView.currentIndex

        TabButton {
            height: parent.height
            text: qsTr("Main")
        }
        TabButton {
            height: parent.height
            text: qsTr("Music")
        }
    }
}


