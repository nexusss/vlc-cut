QT += qml quick multimedia widgets



include(../QmlVlc/QmlVlc.pri)
SOURCES += \
    $$PWD/filelist.cpp \
    $$PWD/vlc-cut.cpp

RESOURCES += \
    $$PWD/vlccut.qrc
# Default rules for deployment.
include(deployment.pri)

QTPLUGIN += qjpeg

HEADERS += \
    $$PWD/filelist.h \
    $$PWD/vlc-cut.h
