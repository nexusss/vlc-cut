
function convertGeom(video,surface,x,y,width,height) {//convert geom from preview to video geom

    var videoWidth = video.width
    var videoHeight = video.height


    var surfaceWidth = surface.width
    var surfaceHeight = surface.height

    var result = {resX: x, resY: y,resW: width,resH: height} ;

    if(videoHeight > 0){
        var prop = surfaceHeight / videoHeight
        result = {resX: x / prop, resY: y / prop,resW: width / prop,resH: height / prop} ;
    }
    return result;
}

