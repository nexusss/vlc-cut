import QtQuick 2.0
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.2

Dialog {
    width: 300
    height: 100
    title: "Text settings"
    standardButtons: StandardButton.Ok | StandardButton.Cancel

    property string fontNamestr: ""
    property string highlightColor: "#f9ee13"
    property bool hightlightState: false
    Rectangle{
        id: rectangle1
        width: 300
        height: 40

        ComboBox {
            id: comboBox1
            y: 8
            height: 20
            anchors.left: parent.left
            anchors.leftMargin: 45
            anchors.right: parent.right
            anchors.rightMargin: 142
            model: Qt.fontFamilies()

            onCurrentTextChanged: {
                fontNamestr = currentText
            }

        }

        Text {
            id: text1
            x: 8
            y: 8
            width: 31
            height: 20
            text: qsTr("Font")
            font.pixelSize: 16
        }

        CheckBox {
            x: 164
            y: 10
            text: qsTr("Highlight text")
            checked: hightlightState
            onCheckedChanged: hightlightState = checked
        }

        Image {
            id: image1
            x: 251
            y: 3
            width: 30
            height: 30
            source: "color_wheel.png"
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    colorDialog.visible = true
                }
            }
        }
    }
    Component.onCompleted: {

        var index = comboBox1.find("Verdana")
        if( index === -1)
            index = comboBox1.find("Arial")

        comboBox1.currentIndex = index
    }
    ColorDialog {
        id: colorDialog

        title: "Please choose a color"
        color: highlightColor
        onAccepted: highlightColor = colorDialog.color
    }
}
