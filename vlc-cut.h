#ifndef VLCCUT_H
#define VLCCUT_H
#include <QObject>
#include <QVector>
#include <QQuickView>
#include <QQuickItem>
#include "filelist.h"

struct SurfaceStruct{
    int x;
    int y;
    int width;
    int height;
    int angle;//угол пооврта в градусах

    struct PreviewRect{
        int x;
        int y;
        int width;
        int height;
    }previewrect;

    qreal startTime;
    qreal stopTime;
    int type;//0 - image; 1 - text; 2 - blur; 3 - music
    struct Text{
        QString text;
        QString color;
        QString fontName;
        bool highLightText;
        QString highLightTextColor;
    }text;
    struct Img{
        QString path;
    }img;
    struct Blur{
        QString ffmpegRaduis;
        QString previewRadius;
    }blur;
    struct Music{
        QString path;
        qreal length;
        QString volume;
    }music;
};

class VlcCutMain : public QObject
{
    Q_OBJECT
    QQuickItem *object;
    QVector <double> curOpenBracketVector,curCloseBracketVector,//current bracket, added from gui
    allOpenBracketVector,allCloseBracketVector;

    FileList *fileList;

    QList <SurfaceStruct> surfaces;
    bool muteMainChannel = false;
    QObject *mainPage;
    QObject *annotationPage;

    QStringList startTime,stopTime;//start cut time, stop cut time
    void cutWithffmpeg(QString fileName);
    void mergeImageAndText(QString fileName);
    qreal videoLength;
public:
    VlcCutMain(QQuickView *engine, QObject *parent = 0);
    QVector <double> getOpenBracket(){return curOpenBracketVector;}
    QVector <double> getCloseBracket(){return curCloseBracketVector;}
    QString getFilePath(){return mainPage->property("videoFilePath").toString();}
    QList <SurfaceStruct> getSurfaces(){return surfaces;}
    qreal getVideoLength(){return videoLength;}
signals:
     void playPl(QVariant startTime,QVariant stopTime,QVariant path);
     void successSignal();
     void videoAdded(QString path);
public slots:
     void init();
     void openPl(QString fileName);

     void addSurface(QRectF surfRect, QRectF previewRect, qreal start, qreal stop, int type);
     void setText(int index, QString text);
     void setImage(int index, QString path);
     void setMusic(int index, QString path,qreal length);

     void setMuteMainChannel(bool state);

     void setVideoLength(qreal length);

     void updateGeomParam(int index, int x, int y, int width, int height,int rotation);
     void updateGeomParamPreview(int index, int x, int y, int width, int height);
     void updateTimeParam(int index, qreal startTime, qreal stopTime);
     void updateTextParam(int index, QString text, QString color, QString fontName,bool highlightState,QString highlightColor);
     void updateRaduis(int index, int radius);
     void updateVolume(int index, int volume);
     void delSurface(int index);
     void updateIndex(int oldIndex, int newIndex);

    void addBracket(bool type, double bracket);
    void undoBracket();
    void createPl(QString filename, double length);
    void clearBracket();
    void clearImageAndText();
    void exportFile(QString fileName, double length);
    void loadNewSettings(QList <SurfaceStruct> surfaces, QString videoPath, QString videoLength, QVector <double> openBracket, QVector <double> closeBracket);
};

#endif // VLCCUT_H
