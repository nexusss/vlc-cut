var visible = true
var play = false

function stopplay() {
    vlc.stop()
}

function checkPlayStatus(volume){
    if(visible === true && play === true){
        vlc.volume = volume
        vlc.unMute()
        vlc.play()
    }
    else{
        vlc.mute()
        vlc.pause()
    }
}

function posChanged(pos){

    var length = vlc.length / 1000.0

    if(pos > length){
        var count = Math.floor(pos / length)//количество аудио помещающихся на данную позицию

        var time = pos - length * count//время проигрывания
        vlc.time = time * 1000
    }
    else
        vlc.time = pos * 1000
}
