
function getFileName(path) {
    if (path) {
        var startIndex = (path.toString().indexOf('\\') >= 0 ? path.toString().lastIndexOf('\\') : path.toString().lastIndexOf('/'));
        var filename = path.toString().substring(startIndex);
        if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
            filename = filename.substring(1);
        }
        return filename
    }
    return ""
}
