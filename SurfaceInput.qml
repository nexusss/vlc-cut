import QtQuick 2.5
//import Qt.labs.controls 1.0
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3
import QtQuick.Dialogs 1.2
import "createBracket.js" as BracketJS

Item {

    id: item1
    width: 420
    height: 65
    property string pathToImage: ""
    property string fullpathToImage: ""

    property string pathToMusic: ""
    property int musicIndex: 0//index in music playlist

    property string text: ""
    property string colorText: "black"

    property int arrayIndex: 0

    property real maxStopTime: 0
    property real maxStartTime: 0
    property real startTimeDefault: 0
    property real stopTimeDefault: 0
    property int type: 0

    signal close(int arrayIndex)
    signal updateTime(real startTime,real stopTime)
    signal updateTextSurface(string text, string colorText,string fontName,bool highlightState,string highlightColor)
    signal updateRadius(int radius)
    signal updateVolumeLevel(int level)
    signal upSignal()
    signal downSignal()

    function setText(text,colorText,fontName,highlightState,highlightColor){
        console.log("set text",colorText)
        item1.text = text
        item1.colorText = colorText
        ts.fontNamestr = fontName
        ts.hightlightState = highlightState
        ts.highlightColor = highlightColor

        updateTextSurface(text,colorText,fontName,highlightState,highlightColor)
    }

    function setRadius(radius){
        radiusBlurSpinBox.value = parseInt(radius)
    }



    onPathToImageChanged: {        
        imageName.text = pathToImage
    }
    onStartTimeDefaultChanged: {
        if(startTimeDefault < startTime.maxTime)
            startTime.defaultValue = startTimeDefault
        else
            startTime.defaultValue = startTime.maxTime
    }
    onStopTimeDefaultChanged: {

        if(stopTimeDefault < stopTime.maxTime)
            stopTime.defaultValue = stopTimeDefault
        else
            stopTime.defaultValue = maxStopTime
    }

    onMaxStopTimeChanged: {
        stopTime.maxTime = maxStopTime
        progressBar.maxValue = maxStopTime
        if(openBracketArray.count > 0){
            console.log("maxStopTime,progressBar.width,progressBar.height,progressBar.maxValue",maxStopTime,progressBar.width,progressBar.height,progressBar.maxValue)
            console.log("BracketJS.convertValueToRect(maxStopTime,progressBar.width,progressBar.height,progressBar.maxValue)",BracketJS.convertValueToRect(maxStopTime,progressBar.width,progressBar.height,progressBar.maxValue))
            openBracketArray.get(0).bracket.maxX = BracketJS.convertValueToRect(maxStopTime,progressBar.width,progressBar.height,progressBar.maxValue)
            closeBracketArray.get(0).bracket.maxX = BracketJS.convertValueToRect(maxStopTime,progressBar.width,progressBar.height,progressBar.maxValue)
        }
    }

    onMaxStartTimeChanged: startTime.maxTime = maxStartTime

    onTextChanged: {
        if(type === 1)
            textField.text = text
    }

    onTypeChanged: stack.currentIndex = type

    onArrayIndexChanged: {
        spin.value = arrayIndex
    }

    onWidthChanged: {
        if(openBracketArray.count > 0){
            BracketJS.changeOpenBracketValue(0,openBracketArray,startTime.seconds,progressBar.width,progressBar.height,progressBar.maxValue)
            BracketJS.changeCloseBracketValue(0,closeBracketArray,stopTime.seconds,progressBar.width,progressBar.height,progressBar.maxValue)
        }
    }

    function getStartTime(){
        return startTime.getSeconds()
    }
    function getStopTime(){
        return stopTime.getSeconds()
    }

    TextSettings{
        id: ts

        width: 300
        height: 100
        onAccepted: updateTextSurface(textField.text,colorText,fontNamestr,hightlightState,highlightColor)
        onHightlightStateChanged: updateTextSurface(textField.text,colorText,fontNamestr,hightlightState,highlightColor)
        onHighlightColorChanged: updateTextSurface(textField.text,colorText,fontNamestr,hightlightState,highlightColor)
    }

    Rectangle {
        id: progressBar
        opacity: 1
        width: parent.width
        height: 22
        color: "#00000000"
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 1
        anchors.left: parent.left
        anchors.leftMargin: 8
        anchors.top: parent.top
        anchors.topMargin: 37
        anchors.right: parent.right
        anchors.rightMargin: 4
        property int maxValue: 0

        Rectangle {

            height: 4
            color: "#cbcbcb"
            radius: 2
            anchors.verticalCenterOffset: -1
            anchors.verticalCenter: parent.verticalCenter
            border.color: "#888888"
            anchors.left: parent.left
            anchors.leftMargin: 0
            anchors.right: parent.right
            anchors.rightMargin: 0
            border.width: 1

            x: 6
            y: 3
        }
    }

    ListModel {
        id: openBracketArray
        property var bracket: 0
    }

    ListModel {
        id: closeBracketArray
        property var bracket: 0
    }


    function clearBracketImage(){
        BracketJS.clearBracket(openBracketArray,closeBracketArray)
    }


    RowLayout {
        height: 32
        anchors.rightMargin: 0
        anchors.leftMargin: 0
        anchors.bottomMargin: 34
        opacity: 1
        anchors.topMargin: 2
        anchors.fill: parent
        spacing: 0

        SpinInput{
            id: spin
            onUp: upSignal()
            onDown: downSignal()
        }

        StackLayout{
            id: stackType
            Layout.fillWidth: false
            Layout.fillHeight: true
            currentIndex: type
            Label {
                id: iLabel
                text: "I"
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                font.pixelSize: 32
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                Layout.fillHeight: true
                Layout.fillWidth: true
                color: "white"
                Layout.minimumWidth: contentWidth + 15
            }

            Label {

                text: "T"
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                font.pixelSize: 32
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                Layout.fillHeight: true
                Layout.fillWidth: true
                color: "white"
                Layout.minimumWidth: contentWidth + 15
                MouseArea{
                    anchors.fill: parent
                    onClicked: ts.open()
                }
            }
            Label {

                text: "B"
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                font.pixelSize: 32
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                Layout.fillHeight: true
                Layout.fillWidth: true
                color: "white"
                Layout.minimumWidth: contentWidth + 15
            }

            Label {

                text: "M"
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                font.pixelSize: 32
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                Layout.fillHeight: true
                Layout.fillWidth: true
                color: "white"
                Layout.minimumWidth: contentWidth + 15
            }
        }



        GridLayout {
            id: gridLayout1
            height: 32
            columnSpacing: 0
            rowSpacing: 0

            Layout.fillHeight: true
            Layout.fillWidth: false
            rows: 2
            columns: 2

            Label {

                width: 55
                height: 32
                text: "Start"
                color: "white"
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                font.pointSize: 8
                Layout.fillHeight: true
                Layout.fillWidth: true
                Layout.row: 0
                Layout.column: 0
            }
            InputTime{
                id: startTime
                colorText: "white"
                width: 100
                height: 32
                fontSize: 8
                maxTime: maxStartTime
                onValueChanged: {
                    BracketJS.changeOpenBracketValue(0,openBracketArray,startTime.seconds,progressBar.width,progressBar.height,progressBar.maxValue)
                    updateTime(startTime.seconds,stopTime.seconds)
                }
                Layout.fillHeight: true
                Layout.fillWidth: true
                Layout.row: 0
                Layout.column: 1
            }
            Label {

                width: 58
                height: 32
                text: "Stop"
                color: "white"
                font.pointSize: 8
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                Layout.fillHeight: true
                Layout.fillWidth: true
                Layout.row: 1
                Layout.column: 0
            }

            InputTime {
                id: stopTime
                colorText: "white"
                width: 100
                height: 32
                fontSize: 8
                maxTime: maxStopTime
                onMaxTimeChanged: {

                    maxStartTime = maxStopTime

                }
                onValueChanged: {
                    maxStartTime = value
                    BracketJS.changeCloseBracketValue(0,closeBracketArray,value,progressBar.width,progressBar.height,progressBar.maxValue)
                    updateTime(startTime.seconds,value)
                }
                Layout.fillHeight: true
                Layout.fillWidth: true
                Layout.row: 1
                Layout.column: 1
            }
        }

        StackLayout{
            id: stack
            currentIndex: type
            RowLayout{//image, arrow type
                Item{
                    Layout.fillHeight: true
                    width: imgPreview.width > 32 ? imgPreview.width : 32
                    Image {
                    id: imgPreview
                    Layout.fillHeight: true
                    width: 20 * sourceSize.width / sourceSize.height > 100 ? 100 : 20 * sourceSize.width / sourceSize.height
                    height: 20
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.verticalCenter: parent.verticalCenter
                    source: fullpathToImage
                    }

                }
                Label {
                    id: imageName
                    height: 32
                    text: pathToImage
                    Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                    Layout.fillHeight: true
                    onTextChanged: {
                        if(text.indexOf("arrow") > -1)
                            iLabel.text = "A"
                    }

                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignRight
                    font.pointSize: 10
                    wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                    color: "white"
                }
            }

            Rectangle {//text type
                color: "#00000000"
                Layout.fillHeight: true
                Layout.fillWidth: true
                RowLayout {
                    anchors.fill: parent
                    spacing: 1
                    Rectangle {
                        id: rectangle1
                        width: 200
                        height: 200
                        color: "#ffffff"
                        Layout.fillHeight: true
                        Layout.fillWidth: true

                        radius: 5

                        TextField {
                            id: textField
                            anchors.fill: parent

                            font.pointSize: 12
                            Layout.fillWidth: true
                            Layout.fillHeight: true
                            onTextChanged: {
                                if(type === 1)
                                    updateTextSurface(text,colorText,ts.fontNamestr,ts.hightlightState,ts.highlightColor)
                            }
                            onCursorVisibleChanged: {
                                if(cursorVisible && text == "Add Text")
                                    text = ""
                            }
                        }
                    }

                    Image{
                        id: colorRect
                        source: "color_wheel.png"
                        width: sourceSize.width
                        height: sourceSize.height

                        Layout.minimumHeight: 6
                        Layout.minimumWidth: 16
                        opacity: 1

                        Layout.fillHeight: true
                        Layout.fillWidth: true

                        MouseArea {
                            anchors.fill: parent
                            onClicked: {
                                colorDialog.visible = true
                            }
                        }

                    }

                }
            }
            RowLayout{//blur type
                RowLayout {
                    anchors.fill: parent
                    Item{
                        Layout.fillHeight: true
                        width: 32
                        Image {
                        height: 20
                        width: 20 * sourceSize.width / sourceSize.height
                        source: "icons/blurIcon.png"
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.verticalCenter: parent.verticalCenter
                        Layout.fillHeight: true
                        }
                    }
                    Label{
                        text: qsTr("Level")
                        color: "white"

                    }
                    SpinBox{
                        id: radiusBlurSpinBox
                        from: 1
                        to:100
                        Layout.fillHeight: true
                        Layout.fillWidth: true
                        value: 50
                        onValueChanged:updateRadius(value)
                    }
                }
            }

            Rectangle{//music type
                color: "#00000000"

                RowLayout {
                    anchors.fill: parent
                    Item{
                        Layout.fillHeight: true
                        width: 32
                        Image {
                        id: musicImage

                        height: 20
                        width: 20 * sourceSize.width / sourceSize.height
                        source: "icons/Music.png"
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.verticalCenter: parent.verticalCenter
                        Layout.fillHeight: true
                        }
                    }
                    Label {
                        id: musicPath


                        text: pathToMusic

                        Layout.fillHeight: true
                        Layout.fillWidth: true

                        verticalAlignment: Text.AlignVCenter
                        horizontalAlignment: Text.AlignHCenter
                        font.pointSize: 10
                        wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                        color: "white"
                    }

                    SpinInput {
                        id: volumeSpin
                        Layout.fillHeight: true
                        maxval: 100
                        minval: 0
                        value: 100
                        onUp: updateVolumeLevel(value)
                        onDown: updateVolumeLevel(value)
                    }
                }
            }
        }

        Button {
            id: closeButton

            width: 32
            height: 32
            Layout.maximumHeight: 32
            Layout.minimumHeight: 32
            Layout.minimumWidth: 32
            Layout.maximumWidth: 32
            Layout.fillHeight: true
            Layout.fillWidth: false
            onClicked: {
                close(arrayIndex)
            }

            Image {
                x: 0
                y: 0
                anchors.fill: parent
                source: "close.png"
            }
        }

    }
    DottedLine{
        y: 62
        height: 3
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.rightMargin: 0

        anchors.leftMargin: 0

    }

    ColorDialog {
        id: colorDialog

        title: "Please choose a color"
        color: colorText
        onAccepted: {
            colorText = colorDialog.color
            updateTextSurface(textField.text,colorText,ts.fontNamestr,ts.hightlightState,ts.highlightColor)
        }

        Component.onCompleted: {
            width = 50
            height = 50
        }
    }

    Component.onCompleted: {
        openBracketArray.append({"bracket":BracketJS.createBracket("BracketOpen.qml",
                                                                   startTime.defaultValue,
                                                                   progressBar.maxValue,
                                                                   progressBar.width,
                                                                   progressBar.height,
                                                                   progressBar,"#00EE00")})
        openBracketArray.get(0).bracket.dragEnabled = true


        openBracketArray.get(0).bracket.posChange.connect(function(x){
            var v = BracketJS.convertXtoValue(x,progressBar.width,progressBar.height,progressBar.maxValue)
            startTime.seconds = v
            closeBracketArray.get(0).bracket.minX = x
            updateTime(v,stopTime.seconds)
        })

        closeBracketArray.append({"bracket":BracketJS.createBracket("BracketClose.qml",
                                                                    stopTime.defaultValue,
                                                                    progressBar.maxValue,
                                                                    progressBar.width,
                                                                    progressBar.height,
                                                                    progressBar,"#00EE00")})

        closeBracketArray.get(0).bracket.dragEnabled = true
        closeBracketArray.get(0).bracket.posChange.connect(function(x){
            var v = BracketJS.convertXtoValue(x,progressBar.width,progressBar.height,progressBar.maxValue)
            stopTime.seconds = v
            maxStartTime = v
            openBracketArray.get(0).bracket.maxX = x
            updateTime(startTime.seconds,v)
        })
    }
}
