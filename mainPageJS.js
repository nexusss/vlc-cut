function addText(newindex,text,startTime,stopTime,rect,angle){

    addSurface(newindex,"","",text,1,startTime,stopTime,rect,angle)
    setText(newindex,text)
}

function addImage(newIndex,path,startTime,stopTime,rect,angle){

    addSurface(newIndex,path,"","",0,startTime,stopTime,rect,angle)
    setPath(newIndex,path)
}

function addBlur(newIndex,startTime,stopTime,rect){

    addSurface(newIndex,"","","",2,startTime,stopTime,rect)

}

function addMusic(newIndex,path,startTime,stopTime,x,y,width,height,angle){

    addSurface(newIndex,"",path,"",3,startTime,stopTime,x,y,width,height,angle)

    setMusicPath(newIndex,path,stopTime - startTime)
}

function addSurface(newIndex,path,mPath,text,type,startTime,stopTime,rect,angle){

    if(rect === undefined) rect = Qt.rect(0,0,50,50)
    if(angle === undefined) angle = 0


    var res = ImageJS.convertGeom(vlcPlayer.video,vlcTransportenSurf,rect.x,rect.y,rect.width,rect.height)
    addSurfaceSignal(Qt.rect(res.resX,res.resY,res.resW,res.resH),rect,startTime,stopTime,type)

    var component = Qt.createComponent("ImageSurface.qml")
    if (component.status === Component.Ready) {
        var childRec = component.createObject(vlcTransportenSurf)
        childRec.type = type
        childRec.x = rect.x
        childRec.y = rect.y
        childRec.width = rect.width
        childRec.height = rect.height
        childRec.rotation = angle

        childRec.startTime = startTime
        childRec.stopTime = stopTime
        childRec.index = newIndex
        childRec.timesChange.connect(function(index,startTime,stopTime){

            updateTimeParamSignal(index,startTime,stopTime)
        })
        childRec.textChange.connect(function(index,text,color,fontName,highlightState,highlightColor){

            updateTextParamSignal(index,text,color,fontName,highlightState,highlightColor)
        })

        childRec.blurChange.connect(function(index,blurLevel){

            updateRaduisSignal(index,blurLevel)
        })

        childRec.geomPreviewChange.connect(function(index,x,y,width,height){
            updateGeomParamPreviewSignal(index,x,y,width,height)
        })

        childRec.geomChange.connect(function(index,x,y,width,height,rotation){
            updateGeomParamPreviewSignal(index,x,y,width,height)
            var res = ImageJS.convertGeom(vlcPlayer.video,vlcTransportenSurf,x,y,width,height)
            updateGeomParamSignal(index,res.resX,res.resY,res.resW,res.resH,rotation)
        })
        if(type === 0)
             childRec.source = path

        if(type === 1)
            childRec.textstring = text

        if(type === 3){
            childRec.musicSource = mPath
            childRec.playstatus = vlcPlayer.playing
        }

        surfaceArray.append({"surf":childRec})
    }

}


